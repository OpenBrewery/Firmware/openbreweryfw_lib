//
// Created by mchodzikiewicz on 10/6/18.
//

#ifndef OPENBREWERYFW_LIB_INTERFACE_H
#define OPENBREWERYFW_LIB_INTERFACE_H


#include <functional>
#include <chrono>
#include "etl/vector.h"
#include "CommInterface.h"

namespace Peripheral {

    template<typename ValueT>
    class AsyncInterface : virtual public CommInterface {
    public:
        AsyncInterface() : CommInterface() {};
        ~AsyncInterface() override = default;

        size_t write(const etl::ivector<ValueT> & packet){
            if(isOpened()){
                return __write(packet);
            } else {
                emit_errorCallback();
                return 0;
            }
        }
        size_t write(const ValueT * buffer, size_t len){
            if(isOpened()){
                return __write(buffer,len);
            } else {
                emit_errorCallback();
                return 0;
            }
        }
        size_t read(etl::ivector<ValueT> & buffer, size_t max_length){
            if(isOpened()){
                return __read(buffer,max_length);
            } else {
                emit_errorCallback();
                return 0;
            }
        }
        size_t read(ValueT * buffer, size_t max_length){
            if(isOpened()){
                return __read(buffer,max_length);
            } else {
                emit_errorCallback();
                return 0;
            }
        }
        virtual size_t bytesAvailable() const __attribute__((pure)) = 0;
//        virtual bool waitForBytesWritten(std::chrono::milliseconds msecs) = 0;
//        virtual bool waitForReadyRead(std::chrono::milliseconds msecs) = 0;


        void setReadyReadCallback(std::function<void(void)> callback){
            readyReadCallback = callback;
        }
        void setBytesWrittenCallback(std::function<void(size_t)> callback){
            bytesWrittenCallback = callback;
        }

    protected:
        virtual size_t __write(const etl::ivector<ValueT> & packet) = 0;
        virtual size_t __write(const ValueT * buffer, size_t len) = 0;
        virtual size_t __read(etl::ivector<ValueT> & buffer, size_t max_length) = 0;
        virtual size_t __read(ValueT * buffer, size_t max_length) = 0;

        void emit_readyReadCallback(){
            if(readyReadCallback) readyReadCallback();
        }
        void emit_bytesWrittenCallback(size_t length){
            if(bytesWrittenCallback) bytesWrittenCallback(length);
        }

    private:
        std::function<void(void)> readyReadCallback;
        std::function<void(size_t)> bytesWrittenCallback;
    };

}

#endif //OPENBREWERYFW_LIB_INTERFACE_H
