//
// Created by mchodzikiewicz on 2/13/19.
//

#ifndef OPENBREWERYFW_LIB_WIFIMANAGER_H
#define OPENBREWERYFW_LIB_WIFIMANAGER_H

#include "WifiClient.h"
#include "WifiServer.h"
#include <memory>

namespace Peripheral {
    class Wifi {
    public:
        Wifi(WifiClient && client, WifiServer && server) : Client(std::move(client)), Server(std::move(server)) {
        }
        bool isInUse();


        std::unique_ptr<WifiClient> getClient();
        std::unique_ptr<WifiServer> getServer();

        void returnClient(std::unique_ptr<WifiClient> client);
        void returnServer(std::unique_ptr<WifiServer> server);

    private:
        bool InUse;

        WifiClient && Client;
        WifiServer && Server;

    };
}


#endif //OPENBREWERYFW_LIB_WIFIMANAGER_H
