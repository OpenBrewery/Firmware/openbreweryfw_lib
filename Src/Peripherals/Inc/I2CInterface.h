//
// Created by mchodzikiewicz on 10/6/18.
//

#ifndef OPENBREWERYFW_LIB_I2CINTERFACE_H
#define OPENBREWERYFW_LIB_I2CINTERFACE_H

#include <memory>
#include "TransactionInterface.h"
#include "ContainerOffsetAccessor.h"

namespace Peripheral {

    using I2CAddress = uint8_t;

    class I2CInterface : virtual public TransactionInterface<uint8_t> {
    public:
        class  Config {
        public:
            enum class Mode {Master,Slave};
            Config(Mode m, int frequency) : mode(m), Frequency(frequency) {}
            Mode mode;
            int Frequency;
        };

        I2CInterface(Config::Mode mode, int freq) : TransactionInterface(), config(mode,freq) {}

        virtual bool setConfig(Config::Mode mode, int frequency) = 0;

        const Config getConfig() {
            return config;
        }

    protected:
        Config config;
    };

    class I2CDevice : public Common::PhysicalResource {
    public:
        I2CDevice(Peripheral::I2CInterface & iface, Peripheral::I2CAddress addr, etl::ivector<uint8_t> & wbuffer, etl::ivector<uint8_t> & rbuffer)
            : Address(addr), Interface(iface), read_buffer(rbuffer), write_buffer(wbuffer), write_buf(write_buffer) {}
        I2CAddress address() { return Address;}

        bool write(){
            write_buffer.at(0) = address();
            return Interface.write(etl::array_view<uint8_t>(write_buffer));
        }

        etl::array_view<uint8_t> request(size_t max_response_len){
            write_buffer.at(0) = address();
            Interface.request(etl::array_view<uint8_t>(write_buffer),read_buffer,max_response_len);
            return etl::array_view<uint8_t>(read_buffer);
        }


    protected:
        Peripheral::I2CAddress Address;
        Peripheral::I2CInterface & Interface;
        etl::ivector<uint8_t> & read_buffer;
    private:
        etl::ivector<uint8_t> & write_buffer;
    protected:
        ContainerOffsetAccessor<etl::ivector<uint8_t>,1,0> write_buf;

    };


}

#endif //OPENBREWERYFW_LIB_I2CINTERFACE_H
