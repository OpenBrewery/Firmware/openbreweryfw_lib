//
// Created by mchodzikiewicz on 10/6/18.
//

#ifndef OPENBREWERYFW_LIB_DIGITALINPUT_H
#define OPENBREWERYFW_LIB_DIGITALINPUT_H

#include "PhysicalResource.h"

namespace Peripheral {

    class DigitalInput : public Common::PhysicalResource {
        ~DigitalInput() = default;
    };
}

#endif //OPENBREWERYFW_LIB_DIGITALINPUT_H
