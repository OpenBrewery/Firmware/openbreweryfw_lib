//
// Created by mchodzikiewicz on 10/6/18.
//

#ifndef OPENBREWERYFW_LIB_SERIALINTERFACE_H
#define OPENBREWERYFW_LIB_SERIALINTERFACE_H

#include "AsyncInterface.h"

namespace Peripheral {
    class SerialInterface : virtual public AsyncInterface<uint8_t> {
    public:
        class Config {
        public:
            enum class Parity {None, Odd, Even};
            enum class StopBits {One, OneAndHalf, Two};
            enum class DataBits {Data5, Data6, Data7, Data8};
            enum class FlowControl {NoFlowControl, HardwareControl, SoftwareControl};
            enum class Mode {Read,Write,ReadWrite,HalfDuplex};

            Config(
                    uint32_t baud,
                    Parity par,
                    StopBits stops,
                    DataBits dataB,
                    FlowControl flowCtrl,
                    Mode mod) : baudrate(baud), parity(par), stopBits(stops), dataBits(dataB), flowControl(flowCtrl), mode(mod) {};
            uint32_t baudrate;
            Parity parity;
            StopBits stopBits;
            DataBits dataBits;
            FlowControl flowControl;
            Mode mode;

        };
        SerialInterface(
                    uint32_t baudrate,
                    Config::Parity parity,
                    Config::StopBits stopBits,
                    Config::DataBits dataBits,
                    Config::FlowControl flowControl,
                    Config::Mode mode) : Config(baudrate,parity,stopBits,dataBits,flowControl,mode) {};
        const Config getConfig() {
            return Config;
        }
        virtual bool setConfig(uint32_t baudrate,
                    Config::Parity parity,
                    Config::StopBits stopBits,
                    Config::DataBits dataBits,
                    Config::FlowControl flowControl,
                    Config::Mode mode) = 0;
    protected:
        Config Config;

    };
}

#endif //OPENBREWERYFW_LIB_SERIALINTERFACE_H
