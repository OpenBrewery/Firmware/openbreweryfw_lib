//
// Created by mchodzikiewicz on 10/6/18.
//

#ifndef OPENBREWERYFW_LIB_ANALOGINPUT_H
#define OPENBREWERYFW_LIB_ANALOGINPUT_H

#include "PhysicalResource.h"

namespace Peripheral {

    class AnalogInput : public Common::PhysicalResource {
        ~AnalogInput() = default;
    };
}

#endif //OPENBREWERYFW_LIB_ANALOGINPUT_H
