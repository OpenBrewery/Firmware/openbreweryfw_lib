//
// Created by mchodzikiewicz on 10/6/18.
//

#ifndef OPENBREWERYFW_LIB_DIGITALOUTPUT_H
#define OPENBREWERYFW_LIB_DIGITALOUTPUT_H

#include "PhysicalResource.h"

namespace Peripheral {
    class DigitalOutput : public Common::PhysicalResource {
        ~DigitalOutput() = default;

    };
}

#endif //OPENBREWERYFW_LIB_DIGITALOUTPUT_H
