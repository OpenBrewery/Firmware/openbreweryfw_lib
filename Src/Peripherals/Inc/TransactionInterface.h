//
// Created by MICHAL on 21.10.2018.
//

#ifndef OPENBREWERYFW_LIB_TRANSACTIONINTERFACE_H
#define OPENBREWERYFW_LIB_TRANSACTIONINTERFACE_H

#include "CommInterface.h"
#include "etl/vector.h"
#include "etl/array_view.h"

namespace Peripheral {
    template<typename ValueT>
    class TransactionInterface : virtual public CommInterface {
    public:
        bool write(etl::array_view<ValueT> data){
            return __write(data);
        }

        constexpr size_t request(etl::array_view<ValueT> request, etl::ivector<ValueT> & response, const size_t max_response_size){
            return __request(request,response, max_response_size);
        }
    protected:
        virtual bool __write(etl::array_view<ValueT> data) = 0;
        virtual size_t __request(etl::array_view<ValueT> request, etl::ivector<ValueT> & response, const size_t max_response_size) = 0;
     };

}

#endif //OPENBREWERYFW_LIB_TRANSACTIONINTERFACE_H
