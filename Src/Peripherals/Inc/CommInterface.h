//
// Created by MICHAL on 21.10.2018.
//

#ifndef OPENBREWERYFW_LIB_COMMINTERFACE_H
#define OPENBREWERYFW_LIB_COMMINTERFACE_H

#include <PhysicalResource.h>
#include <functional>

namespace Peripheral {
    class CommInterface : Common::PhysicalResource {
    public:
        CommInterface() : Common::PhysicalResource() {};
        ~CommInterface() override = default;

        virtual bool open() = 0;

        virtual void close() = 0;

        virtual bool isOpened() const = 0;


        void setErrorCallback(std::function<void(void)>);
        void setConnectedCallback(std::function<void(void)>);
        void setDisconnectedCallback(std::function<void(void)>);


    protected:
        void emit_errorCallback();
        void emit_connectedCallback();
        void emit_disconnectedCallback();
    private:
        std::function<void(void)> errorCallback;
        std::function<void(void)> connectedCallback;
        std::function<void(void)> disconnectedCallback;
    };
}


#endif //OPENBREWERYFW_LIB_COMMINTERFACE_H
