//
// Created by mchodzikiewicz on 2/13/19.
//

#ifndef OPENBREWERYFW_LIB_WIFISERVER_H
#define OPENBREWERYFW_LIB_WIFISERVER_H

#include <etl/string_view.h>
#include <etl/message_timer.h>
#include <chrono>
#include <stdio.h>

namespace Peripheral {


    class WifiServerNotInUseTimeoutMessage : public etl::message<123> {};

    //TODO (mchodzikiewicz): this should be rewritten to state machine
    class WifiServer : public etl::message_router<WifiServer,WifiServerNotInUseTimeoutMessage> {
    public:

        explicit WifiServer(etl::imessage_timer & timer) : message_router(0)
                , OnStartedServingCallback(nullptr)
                , OnStoppedServingCallback(nullptr)
                , OnClientConnectedCallback(nullptr)
                , OnClientDisconnectedCallback(nullptr)
                , OnNotInUseCallback(nullptr)
                , MessageTimer(timer)
                , NotInUseTimeout(std::chrono::seconds(0))
                , IsTimeoutSet(false)
                , timeoutTimerHandle(0) {}

        virtual bool init() = 0;

        virtual bool setSSID(etl::string_view ssid) = 0;

        virtual bool setPassword(etl::string_view passwd) = 0;

        virtual etl::string_view getSSID() = 0;

        virtual etl::string_view getPassword() = 0;

        virtual bool serve() { //All overrides of this method are supposed to launch base function to trigger timeout
            if(IsTimeoutSet){
                setNotInUseTimeoutTimer();
            }
            onStartedServingCallback();
            return true;
        };

        virtual void stop() { //All overrides of this method are supposed to launch base function to trigger timeout
                onStoppedServingCallback();
        };

        virtual bool isServing() = 0;

        bool setUnusedTimeout(std::chrono::seconds timeout) {
            if(IsTimeoutSet) return false;

            NotInUseTimeout = timeout;
            IsTimeoutSet = true;
            setNotInUseTimeoutTimer();
            return true;
        }

        std::chrono::seconds getUnusedTimeout() {
            using namespace std::chrono_literals;
            if(IsTimeoutSet) return NotInUseTimeout;
            else return 0s;
        }

        void cancelUnusedTimeout(){
            MessageTimer.unregister_timer(timeoutTimerHandle);
            IsTimeoutSet = false;
        }

        //these three should be called by hardware callbacks
        void onStartedServingCallback(){ if(OnStartedServingCallback != nullptr) OnStartedServingCallback();}
        void onStoppedServingCallback(){ if(OnStoppedServingCallback != nullptr) OnStoppedServingCallback();}
        void onClientConnectedCallback(){ if(OnClientConnectedCallback != nullptr) OnClientConnectedCallback();}
        void onClientDisconnectedCallback(){ if(OnClientDisconnectedCallback != nullptr) OnClientDisconnectedCallback();}


        void setOnStartedServingCallback(std::function<void()> && f){OnStartedServingCallback = f;}
        void setOnStoppedServingCallback(std::function<void()> && f){OnStoppedServingCallback = f;}
        void setOnClientConnectedCallback(std::function<void()> && f){OnClientConnectedCallback = f;}
        void setOnClientDisconnectedCallback(std::function<void()> && f){OnClientDisconnectedCallback = f;}
        void setOnNotInUseCallback(std::function<void()> && f){OnNotInUseCallback = f;}


        void on_receive(etl::imessage_router& sender, const WifiServerNotInUseTimeoutMessage& msg)  {
            if(IsTimeoutSet) {
                stop();
                if(OnNotInUseCallback != nullptr) OnNotInUseCallback();
            }
        }

        void on_receive_unknown(etl::imessage_router& sender, const etl::imessage& msg)  {

        }


    private:
        void setNotInUseTimeoutTimer() {
            timeoutTimerHandle =  MessageTimer.register_timer(
                    NotInUseTimeoutMessage
                    ,*this
                    ,std::chrono::duration_cast<std::chrono::milliseconds>(NotInUseTimeout).count()
                    , etl::timer::mode::SINGLE_SHOT);
            MessageTimer.start(timeoutTimerHandle);
        }

        std::function<void()> OnStartedServingCallback;
        std::function<void()> OnStoppedServingCallback;
        std::function<void()> OnClientConnectedCallback;
        std::function<void()> OnClientDisconnectedCallback;
        std::function<void()> OnNotInUseCallback;

        etl::imessage_timer & MessageTimer;    //this timer is supposed to tick each ms
        std::chrono::seconds NotInUseTimeout;

        const WifiServerNotInUseTimeoutMessage NotInUseTimeoutMessage;
        bool IsTimeoutSet;
        etl::timer::id::type timeoutTimerHandle;

    };
}

#endif //OPENBREWERYFW_LIB_WIFISERVER_H
