//
// Created by mchodzikiewicz on 2/2/19.
//

#ifndef OPENBREWERYFW_LIB_WIFICLIENT_H
#define OPENBREWERYFW_LIB_WIFICLIENT_H

#include <etl/string_view.h>
#include <etl/message_timer.h>
#include <chrono>
#include <optional>
#include <etl/cstring.h>

namespace Peripheral {
    class ConnNotSucceededTimeoutMsg : public etl::message<124> {};


    //TODO (mchodzikiewicz): this should be rewritten to state machine
    class WifiClient : public etl::message_router<WifiClient,ConnNotSucceededTimeoutMsg> {
    public:
        explicit WifiClient(etl::imessage_timer & timer) : message_router(0), MessageTimer(timer), NotSucceededTimeout(std::chrono::seconds(0)), IsTimeoutSet(false), timeoutTimerHandle(0) {}

        virtual bool init() = 0;

        virtual bool setSSID(etl::string_view ssid) = 0;

        virtual bool setPassword(etl::string_view passwd) = 0;

        virtual etl::string_view getSSID() = 0;

        virtual etl::string_view getPassword() = 0;

        virtual std::optional<etl::string_view> getIP(etl::istring & ip_buf) = 0;

        virtual bool connect() { //All overrides of this method are supposed to launch base function to trigger timeout
            if(IsTimeoutSet){
                setNotSucceededTimeoutTimer();
            }
            return true;
        };

        virtual void disconnect() = 0;

        virtual bool isConnected() = 0;

        //these three should be called by hardware callbacks
        virtual void onInitialized(){ if(OnInitializedCallback != nullptr) OnInitializedCallback();}
        virtual void onConnected(){ if(OnConnectedCallback != nullptr) OnConnectedCallback();}
        virtual void onDisconnected(){ if(OnDisconnectedCallback != nullptr) OnDisconnectedCallback();}


        void setOnInitializedCallback(std::function<void()> && f){OnInitializedCallback = f;}
        void setOnConnectedCallback(std::function<void()> && f){OnConnectedCallback = f;}
        void setOnDisconnectedCallback(std::function<void()> && f){OnDisconnectedCallback = f;}
        void setOnNotSucceededTimeoutCallback(std::function<void()> && f){OnNotSucceededTimeoutCallback = f;}

        bool setNotSucceededTimeout(std::chrono::seconds timeout) {
            NotSucceededTimeout = timeout;
            IsTimeoutSet = true;
            setNotSucceededTimeoutTimer();
            return true;
        }

        void cancelUnusedTimeout(){
            MessageTimer.unregister_timer(timeoutTimerHandle);
            IsTimeoutSet = false;
        }


        void on_receive(etl::imessage_router& sender, const ConnNotSucceededTimeoutMsg& msg)  {
            if(IsTimeoutSet) {
                disconnect();
                if(OnNotSucceededTimeoutCallback != nullptr) OnNotSucceededTimeoutCallback();
            }
        }

        void on_receive_unknown(etl::imessage_router& sender, const etl::imessage& msg)  {

        }

    private:
        void setNotSucceededTimeoutTimer() {
            timeoutTimerHandle = MessageTimer.register_timer(
                    connNotSucceededTimeoutMsg
                    ,*this
                    ,std::chrono::duration_cast<std::chrono::milliseconds>(NotSucceededTimeout).count()
                    , etl::timer::mode::SINGLE_SHOT);
            MessageTimer.start(timeoutTimerHandle);
        }

        std::function<void()> OnInitializedCallback;
        std::function<void()> OnConnectedCallback;
        std::function<void()> OnDisconnectedCallback;
        std::function<void()> OnNotSucceededTimeoutCallback;

        etl::imessage_timer & MessageTimer;    //this timer is supposed to tick each ms
        std::chrono::seconds NotSucceededTimeout;

        const ConnNotSucceededTimeoutMsg connNotSucceededTimeoutMsg;
        bool IsTimeoutSet;
        etl::timer::id::type timeoutTimerHandle;
    };
}

#endif //OPENBREWERYFW_LIB_WIFICLIENT_H
