//
// Created by mchodzikiewicz on 10/6/18.
//

#ifndef OPENBREWERYFW_LIB_ANALOGOUTPUT_H
#define OPENBREWERYFW_LIB_ANALOGOUTPUT_H

#include "PhysicalResource.h"

namespace Peripheral {
    class AnalogOutput : public Common::PhysicalResource {
        ~AnalogOutput() = default;
    };
}

#endif //OPENBREWERYFW_LIB_ANALOGOUTPUT_H
