//
// Created by mchodzikiewicz on 10/6/18.
//

#include "DummyAsyncInterface.h"

bool Peripheral::DummyAsyncInterface::open() {
    if (Opened){
        emit_errorCallback();
        return false;
    }
    Opened = true;
    emit_connectedCallback();
    return Opened;
}

void Peripheral::DummyAsyncInterface::close() {
    if(Opened){
        emit_disconnectedCallback();
    }
    Opened = false;
}

bool Peripheral::DummyAsyncInterface::isOpened() const {
    return Opened;
}


size_t Peripheral::DummyAsyncInterface::__write(const etl::ivector<uint8_t> &packet) {
    for(uint8_t byte : packet){
        writeBuffer.push_back(byte);
    }
    emit_bytesWrittenCallback(packet.size());
    return packet.size();
}

size_t Peripheral::DummyAsyncInterface::__write(const uint8_t *buffer, size_t len) {
    for(int i=0; i<len;i++){
        writeBuffer.push_back(buffer[i]);
    }
    emit_bytesWrittenCallback(len);
    return len;
}

size_t Peripheral::DummyAsyncInterface::__read(etl::ivector<uint8_t> &buffer, const size_t max_length) {
    size_t copied_size = std::min(max_length, readBuffer.size());
    std::copy(
        readBuffer.begin(),
        readBuffer.begin() + copied_size,
        std::back_inserter(buffer)
        );
    readBuffer.erase(readBuffer.begin(),readBuffer.begin()+copied_size);
    return copied_size;
}

size_t Peripheral::DummyAsyncInterface::__read(uint8_t * buffer, const size_t max_length) {
    size_t copied_size = std::min(max_length, readBuffer.size());
    std::copy(
            readBuffer.begin(),
            readBuffer.begin() + copied_size,
            &buffer[readBuffer.size()]
    );
    readBuffer.erase(readBuffer.begin(),readBuffer.begin()+copied_size);
    return copied_size;
}

size_t Peripheral::DummyAsyncInterface::bytesAvailable() const {
    return readBuffer.size();
}

//bool Peripheral::DummyAsyncInterface::waitForBytesWritten(std::chrono::milliseconds msecs) {
//    return false;
//}
//
//bool Peripheral::DummyAsyncInterface::waitForReadyRead(std::chrono::milliseconds msecs) {
//    return false;
//}

void Peripheral::DummyAsyncInterface::call_readyRead() {
    emit_readyReadCallback();
}
