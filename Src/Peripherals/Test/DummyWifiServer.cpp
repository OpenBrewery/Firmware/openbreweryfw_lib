//
// Created by mchodzikiewicz on 2/13/19.
//

#include "DummyWifiServer.h"

bool Peripheral::DummyWifiServer::init() {
    return false;
}

bool Peripheral::DummyWifiServer::setSSID(etl::string_view ssid) {
    SSID.assign(ssid.cbegin(),ssid.cend());
    return true;
}

bool Peripheral::DummyWifiServer::setPassword(etl::string_view passwd) {
    Password.assign(passwd.cbegin(),passwd.cend());
    return true;
}

etl::string_view Peripheral::DummyWifiServer::getSSID() {
    return etl::string_view(SSID.c_str(),SSID.size());
}

etl::string_view Peripheral::DummyWifiServer::getPassword() {
    return etl::string_view(Password.c_str(),Password.size());
}

bool Peripheral::DummyWifiServer::serve() {
    if(IsServing) return false;
    IsServing = true;
    WifiServer::serve();
    return true;
}

void Peripheral::DummyWifiServer::stop() {
    IsServing = false;
    WifiServer::stop();
}

bool Peripheral::DummyWifiServer::isServing() {
    return IsServing;
}
