//
// Created by mchodzikiewicz on 2/2/19.
//

#include "DummyWifiClient.h"

using namespace Peripheral;

bool DummyWifiClient::setSSID(etl::string_view ssid) {
    SSID.assign(ssid.cbegin(),ssid.cend());
    return true;
}


bool DummyWifiClient::setPassword(etl::string_view passwd) {
    Passwd.assign(passwd.cbegin(),passwd.cend());
    return true;
}

bool DummyWifiClient::connect() {
    if(!Connected){
        WifiClient::connect();
        if(!ConnectionOnHold){
            onConnected();
            return Connected = true;
        } else {
            TryingToConnect = true;
        }
    }
    return false;
}

void DummyWifiClient::disconnect() {
    onDisconnected();
    Connected = false;
}

etl::string_view DummyWifiClient::getSSID() {
    return etl::string_view(SSID.c_str(),SSID.size());
}

etl::string_view DummyWifiClient::getPassword() {
    return etl::string_view(Passwd.c_str(),Passwd.size());
}

bool DummyWifiClient::isConnected() {
    return Connected;
}

bool DummyWifiClient::init() {
    Initialized = true;
    return true;
}

void DummyWifiClient::holdConnection(bool hold) {
    ConnectionOnHold = hold;
    if(!hold && TryingToConnect) {
        Connected = true;

    }
}

std::optional<etl::string_view> DummyWifiClient::getIP(etl::istring & ip_buf) {
    if(isConnected()) {
        ip_buf.assign("192.168.0.1");
        return etl::string_view(ip_buf.c_str());
    } else {
        return std::nullopt;
    }
}
