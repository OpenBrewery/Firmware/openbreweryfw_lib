//
// Created by mchodzikiewicz on 10/7/18.
//

#include "gtest/gtest.h"
#include "DummyAsyncInterface.h"

class DummyInterfaceTest : public ::testing::Test {
public:
    void SetUp() override {
        errors_triggered = 0;
        interface = std::make_unique<Peripheral::DummyAsyncInterface>();
        interface->setErrorCallback([this](){errors_triggered++;});
    };
    void TearDown() override {
        if(errors_triggered){
            int errs = errors_triggered;
            errors_triggered = 0;
            FAIL() << "Unexpected errors: " << errs;
        }
    };

    std::unique_ptr<Peripheral::DummyAsyncInterface> interface;
    int errors_triggered;
};

TEST_F(DummyInterfaceTest, InitNoArgs) {
    EXPECT_FALSE(interface->isOpened());
}

TEST_F(DummyInterfaceTest, OpenInterfaceNoConnectedCallback) {
    EXPECT_FALSE(interface->isOpened());
    EXPECT_TRUE(interface->open());
    EXPECT_TRUE(interface->isOpened());
}

TEST_F(DummyInterfaceTest, OpenInterfaceConnectedCallback) {
    int connected = 0;
    interface->setConnectedCallback([&connected](){connected++;});
    EXPECT_FALSE(interface->isOpened());
    EXPECT_TRUE(interface->open());
    EXPECT_TRUE(interface->isOpened());
    EXPECT_EQ(connected,1);
}

TEST_F(DummyInterfaceTest, OpenInterfaceTwice){
    EXPECT_TRUE(interface->open());
    EXPECT_TRUE(interface->isOpened());
    EXPECT_FALSE(interface->open());
    EXPECT_EQ(errors_triggered,1);
    errors_triggered = 0;
}

TEST_F(DummyInterfaceTest, CloseInterface) {
    EXPECT_FALSE(interface->isOpened());
    EXPECT_TRUE(interface->open());
    EXPECT_TRUE(interface->isOpened());
    interface->close();
    EXPECT_FALSE(interface->isOpened());
}

TEST_F(DummyInterfaceTest, CloseInterfaceDisconnectedCallback) {
    int disconnected = 0;
    interface->setDisconnectedCallback([&disconnected](){disconnected++;});
    EXPECT_FALSE(interface->isOpened());
    EXPECT_TRUE(interface->open());
    EXPECT_TRUE(interface->isOpened());
    EXPECT_EQ(disconnected,0);
    interface->close();
    EXPECT_FALSE(interface->isOpened());
    EXPECT_EQ(disconnected,1);
}


TEST_F(DummyInterfaceTest, CloseInterfaceTwice) {
    EXPECT_FALSE(interface->isOpened());
    EXPECT_TRUE(interface->open());
    EXPECT_TRUE(interface->isOpened());
    interface->close();
    EXPECT_FALSE(interface->isOpened());
    interface->close();
    EXPECT_FALSE(interface->isOpened());
}

TEST_F(DummyInterfaceTest, WriteToClosedInterface) {
    int error_count = 0;
    interface->setErrorCallback([&error_count](){error_count++;});
    EXPECT_EQ(interface->writeBuffer.size(),0);
    uint8_t buffer[] = {"TEST"};
    EXPECT_EQ(error_count,0);
    EXPECT_EQ(0, interface->write(buffer,5));
    EXPECT_EQ(error_count,1);
    EXPECT_EQ(interface->writeBuffer.size(), 0);
}

TEST_F(DummyInterfaceTest, WriteWithoutCallbackBinded) {
    interface->open();
    EXPECT_EQ(interface->writeBuffer.size(),0);
    uint8_t buffer[] = {"TEST"};
    EXPECT_EQ(5, interface->write(buffer,5));
    EXPECT_EQ(interface->writeBuffer.size(), 5);
    EXPECT_EQ(std::string{"TEST"}, std::string(interface->writeBuffer.begin(),interface->writeBuffer.end()-1));
}

TEST_F(DummyInterfaceTest, WriteWithCallback) {
    int bytes_written_callback_count = 0;
    size_t last_bytes_written_provided = 0;
    interface->open();
    interface->setBytesWrittenCallback(
            [&bytes_written_callback_count,&last_bytes_written_provided](size_t bytes){
                last_bytes_written_provided=bytes;
                bytes_written_callback_count++;
            });
    EXPECT_EQ(interface->writeBuffer.size(),0);
    EXPECT_EQ(bytes_written_callback_count,0);
    EXPECT_EQ(last_bytes_written_provided,0);
    EXPECT_EQ(5, interface->write((uint8_t *) "TEST", 5));
    EXPECT_EQ(interface->writeBuffer.size(), 5);
    EXPECT_EQ(bytes_written_callback_count,1);
    EXPECT_EQ(last_bytes_written_provided,5);
    EXPECT_EQ(std::string{"TEST"}, std::string(interface->writeBuffer.begin(),interface->writeBuffer.end()-1));
}

TEST_F(DummyInterfaceTest, ReadAllFromClosedInterface){

    EXPECT_FALSE(interface->isOpened());
    etl::vector<uint8_t,10> buffer;
    interface->read(buffer,10);
    EXPECT_EQ(errors_triggered,1);
    errors_triggered = 0;
}

TEST_F(DummyInterfaceTest, ReadAll){
    int ready_read_triggered = 0;
    interface->open();
    std::vector<uint8_t> read_packet{'T','E','S','T'};
    interface->readBuffer.insert(interface->readBuffer.end(),read_packet.begin(),read_packet.end());
    interface->setReadyReadCallback([&ready_read_triggered](){ready_read_triggered++;});
    interface->call_readyRead();
    EXPECT_EQ(4, interface->bytesAvailable());
    etl::vector<uint8_t,10> buffer;
    interface->read(buffer,10);
    EXPECT_EQ(0, interface->bytesAvailable());
    EXPECT_EQ("TEST", std::string(buffer.begin(),buffer.end()));
}

TEST_F(DummyInterfaceTest, ReadPartOfPacket)
{
    interface->open();
    int ready_read_triggered = 0;
    std::vector<uint8_t> read_packet{'T','E','S','T'};
    interface->readBuffer.insert(interface->readBuffer.end(),read_packet.begin(),read_packet.end());
    interface->setReadyReadCallback([&ready_read_triggered](){ready_read_triggered++;});
    interface->call_readyRead();
    EXPECT_EQ(4, interface->bytesAvailable());
    etl::vector<uint8_t,10> buffer;
    interface->read(buffer,3);
    EXPECT_EQ(1, interface->bytesAvailable());
    EXPECT_EQ("TES", std::string(buffer.begin(),buffer.end()));
}