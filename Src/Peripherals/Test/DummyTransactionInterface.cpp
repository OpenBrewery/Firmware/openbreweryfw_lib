//
// Created by MICHAL on 21.10.2018.
//

#include "DummyTransactionInterface.h"


size_t
Peripheral::DummyTransactionInterface::__request(etl::array_view<uint8_t> request, etl::ivector<uint8_t> &response, const size_t max_response_len) {
    response.clear();
    Request.clear();
    std::copy(request.begin(),request.end(),std::back_inserter(Request));
    std::copy_if(
            Response.begin(),
            Response.end(),
            std::back_inserter(response),
            [max = response.max_size()](uint8_t val){
                static size_t already_copied = 0;
                already_copied++;
                return already_copied < max;
            }
            );
    return response.size();
}

bool Peripheral::DummyTransactionInterface::__write(etl::array_view<uint8_t> request) {
    std::copy(request.begin(),request.end(),std::back_inserter(Request));
    return true;
}

