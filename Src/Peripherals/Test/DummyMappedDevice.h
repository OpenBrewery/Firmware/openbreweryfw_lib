//
// Created by michal on 02.12.18.
//

#ifndef OPENBREWERYFW_LIB_DUMMYMAPPEDDEVICE_H
#define OPENBREWERYFW_LIB_DUMMYMAPPEDDEVICE_H

#include <unordered_map>
#include <experimental/optional>

using namespace std::experimental;

namespace Peripheral {
    template<typename AddressT, typename ValueT>
    class DummyMappedDevice {
    public:
        optional<ValueT> get(AddressT addr){
            return Map.find(addr) != Map.end() ? Map.at(addr) : nullopt;
        }

        void set(AddressT addr, ValueT val){
            Map.insert_or_assign(addr,val);
        }


    private:
        std::unordered_map <AddressT, ValueT> Map;

    };
}


#endif //OPENBREWERYFW_LIB_DUMMYMAPPEDDEVICE_H
