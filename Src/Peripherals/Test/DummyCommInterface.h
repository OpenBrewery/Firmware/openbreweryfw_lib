//
// Created by michal on 24.10.18.
//

#ifndef OPENBREWERYFW_LIB_DUMMYCOMMINTERFACE_H
#define OPENBREWERYFW_LIB_DUMMYCOMMINTERFACE_H

#include <CommInterface.h>

namespace Peripheral {
    class DummyCommInterface : virtual public CommInterface {
    public:
        bool open() override;

        void close() override;

        bool isOpened() const override;

    private:

        bool Opened = false;

    };
}

#endif //OPENBREWERYFW_LIB_DUMMYCOMMINTERFACE_H
