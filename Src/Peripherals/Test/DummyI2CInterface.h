//
// Created by michal on 14.10.18.
//

#ifndef OPENBREWERYFW_LIB_DUMMYI2CINTERFACE_H
#define OPENBREWERYFW_LIB_DUMMYI2CINTERFACE_H

#include "I2CInterface.h"
#include "DummyTransactionInterface.h"
#include "DummyMappedDevice.h"

namespace Peripheral {

    class DummyI2CInterface : virtual public I2CInterface, virtual public DummyTransactionInterface {
    public:
        DummyI2CInterface() : I2CInterface(I2CInterface::Config::Mode::Master, 100000), DummyTransactionInterface() {}

        bool setConfig(Config::Mode mode, int frequency) override {
            config.mode = mode;
            config.Frequency = frequency;
            return true;
        }

    };

}
#endif //OPENBREWERYFW_LIB_DUMMYI2CINTERFACE_H
