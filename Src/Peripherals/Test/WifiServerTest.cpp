//
// Created by mchodzikiewicz on 2/13/19.
//

#include "gtest/gtest.h"
#include "DummyWifiServer.h"
#include <chrono>

using namespace Peripheral;

class WifiServerTest : public ::testing::Test {
public:
    void SetUp() {
        timer = new etl::message_timer<1>;
    };
    void TearDown() {

    };

    etl::imessage_timer * timer;
};

TEST_F(WifiServerTest, Init) {
    DummyWifiServer server(*timer);
    EXPECT_EQ(server.getSSID().empty(),true);
    EXPECT_EQ(server.getPassword().empty(),true);
    EXPECT_EQ(server.isServing(),false);
}

TEST_F(WifiServerTest, SetSSID) {
    DummyWifiServer server(*timer);
    std::string ssid = "Hello";
    EXPECT_EQ(server.setSSID(etl::string_view(ssid.c_str(),ssid.size())),true);
    EXPECT_STREQ(server.getSSID().cbegin(),"Hello");

}

TEST_F(WifiServerTest, SetPasswd) {
    DummyWifiServer server(*timer);
    std::string passwd = "Hello";
    EXPECT_EQ(server.setPassword(etl::string_view(passwd.c_str(),passwd.size())),true);
    EXPECT_STREQ(server.getPassword().cbegin(),"Hello");
}

TEST_F(WifiServerTest, Connect){
    DummyWifiServer server(*timer);
    EXPECT_TRUE(server.serve());
    EXPECT_TRUE(server.isServing());
}

TEST_F(WifiServerTest, ConnectWhenConnected){
    DummyWifiServer server(*timer);
    EXPECT_TRUE(server.serve());
    EXPECT_FALSE(server.serve());
    EXPECT_TRUE(server.isServing());
}

TEST_F(WifiServerTest, OnStartedServing){
    DummyWifiServer server(*timer);
    int startedServingCalled = 0;
    server.setOnStartedServingCallback([&startedServingCalled](){startedServingCalled++;});
    EXPECT_TRUE(server.serve());
    EXPECT_TRUE(server.isServing());
    EXPECT_EQ(startedServingCalled,1);
}

TEST_F(WifiServerTest, OnStoppedServing){
    DummyWifiServer server(*timer);
    int stoppedServingCalled = 0;
    server.setOnStoppedServingCallback([&stoppedServingCalled](){stoppedServingCalled++;});
    EXPECT_TRUE(server.serve());
    EXPECT_EQ(stoppedServingCalled,0);
    server.stop();
    EXPECT_EQ(stoppedServingCalled,1);
}

TEST_F(WifiServerTest, OnStartedServingNoCallback){
    DummyWifiServer server(*timer);
    int startedServingCalled = 0;
    EXPECT_TRUE(server.serve());
    EXPECT_TRUE(server.isServing());
    EXPECT_EQ(startedServingCalled,0);
}

TEST_F(WifiServerTest, OnClientConnectedDisconnected){
    DummyWifiServer server(*timer);
    EXPECT_TRUE(server.serve());
    int onClientConnectedCalled = 0;
    int onClientDisconnectedCalled = 0;
    server.setOnClientConnectedCallback([&onClientConnectedCalled](){onClientConnectedCalled++;});
    server.setOnClientDisconnectedCallback([&onClientDisconnectedCalled](){onClientDisconnectedCalled++;});
    server.onClientConnectedCallback();
    EXPECT_EQ(onClientConnectedCalled,1);
    EXPECT_EQ(onClientDisconnectedCalled,0);
    server.onClientDisconnectedCallback();
    EXPECT_EQ(onClientDisconnectedCalled,1);
    EXPECT_EQ(onClientConnectedCalled,1);
}

TEST_F(WifiServerTest, OnClientConnectedDisconnectedNoCallbacks){
    DummyWifiServer server(*timer);
    EXPECT_TRUE(server.serve());
    int onClientConnectedCalled = 0;
    int onClientDisconnectedCalled = 0;
    server.onClientConnectedCallback();
    EXPECT_EQ(onClientConnectedCalled,0);
    EXPECT_EQ(onClientDisconnectedCalled,0);
    server.onClientDisconnectedCallback();
    EXPECT_EQ(onClientDisconnectedCalled,0);
    EXPECT_EQ(onClientConnectedCalled,0);
}

TEST_F(WifiServerTest, OnUnusedTimeout5s){
    using namespace std::chrono_literals;
    DummyWifiServer server(*timer);
    timer->enable(true);
    int stoppedServingCalled = 0;
    server.setOnStoppedServingCallback([&stoppedServingCalled](){stoppedServingCalled++;});
    EXPECT_TRUE(server.serve());
    server.setUnusedTimeout(5s);
    timer->tick(4999);
    EXPECT_EQ(stoppedServingCalled,0);
    EXPECT_TRUE(server.isServing());
    timer->tick(1);
    EXPECT_FALSE(server.isServing());
    EXPECT_EQ(stoppedServingCalled,1);
}

TEST_F(WifiServerTest, OnUnusedTimeout500s){
    using namespace std::chrono_literals;
    DummyWifiServer server(*timer);
    timer->enable(true);
    EXPECT_TRUE(server.serve());
    server.setUnusedTimeout(500s);
    timer->tick(499999);
    EXPECT_TRUE(server.isServing());
    timer->tick(1);
    EXPECT_FALSE(server.isServing());
}


TEST_F(WifiServerTest, NoUnusedTimeout10h){
    using namespace std::chrono_literals;
    DummyWifiServer server(*timer);
    timer->enable(true);
    EXPECT_TRUE(server.serve());
    timer->tick(std::chrono::duration_cast<std::chrono::milliseconds>(10h).count());
    EXPECT_TRUE(server.isServing());
}

TEST_F(WifiServerTest, CancelUnusedTimeout500s){
    using namespace std::chrono_literals;
    DummyWifiServer server(*timer);
    timer->enable(true);
    EXPECT_TRUE(server.serve());
    server.setUnusedTimeout(500s);
    timer->tick(499999);
    EXPECT_TRUE(server.isServing());
    server.cancelUnusedTimeout();
    timer->tick(1);
    EXPECT_TRUE(server.isServing());
}

TEST_F(WifiServerTest, getCurrentTimeout){
    using namespace std::chrono_literals;
    DummyWifiServer server(*timer);
    timer->enable(true);
    EXPECT_TRUE(server.serve());
    EXPECT_TRUE(server.setUnusedTimeout(500s));
    EXPECT_EQ(500s,server.getUnusedTimeout());
}

TEST_F(WifiServerTest, changeTimeoutWhenSet){
    using namespace std::chrono_literals;
    DummyWifiServer server(*timer);
    timer->enable(true);
    EXPECT_TRUE(server.serve());
    EXPECT_TRUE(server.setUnusedTimeout(500s));
    EXPECT_EQ(500s,server.getUnusedTimeout());
    EXPECT_FALSE(server.setUnusedTimeout(30s));
    EXPECT_EQ(500,server.getUnusedTimeout().count());
    server.cancelUnusedTimeout();
    EXPECT_TRUE(server.setUnusedTimeout(30s));
    EXPECT_EQ(30,server.getUnusedTimeout().count());
}