//
// Created by mchodzikiewicz on 2/2/19.
//

#include "gtest/gtest.h"
#include "DummyWifiClient.h"

using namespace Peripheral;

class WifiClientTest : public ::testing::Test {
public:
    void SetUp() {
        timer = new etl::message_timer<1>;
        client = new DummyWifiClient(*timer);
    };
    void TearDown() {

    };

    etl::imessage_timer * timer;

    DummyWifiClient * client;
    etl::string<50> ip_buf;
};

TEST_F(WifiClientTest, Init) {
     EXPECT_EQ(client->getSSID().empty(),true);
     EXPECT_EQ(client->getPassword().empty(),true);
     EXPECT_EQ(client->isConnected(),false);
     EXPECT_EQ(client->getIP(ip_buf),std::nullopt);
}


TEST_F(WifiClientTest, SetSSID) {
     std::string ssid = "Hello";
     EXPECT_EQ(client->setSSID(etl::string_view(ssid.c_str(),ssid.size())),true);
     EXPECT_STREQ(client->getSSID().cbegin(),"Hello");

}

TEST_F(WifiClientTest, SetPasswd) {
     std::string passwd = "Hello";
     EXPECT_EQ(client->setPassword(etl::string_view(passwd.c_str(),passwd.size())),true);
     EXPECT_STREQ(client->getPassword().cbegin(),"Hello");
}

TEST_F(WifiClientTest, Connect){
     EXPECT_TRUE(client->connect());
     EXPECT_TRUE(client->isConnected());
     EXPECT_STREQ(client->getIP(ip_buf).value().cbegin(),"192.168.0.1");
}

TEST_F(WifiClientTest, ConnectWithCallback){
    int connectedCalled = 0;
    client->setOnConnectedCallback([&connectedCalled](){connectedCalled++;});
    EXPECT_TRUE(client->connect());
    EXPECT_TRUE(client->isConnected());
    EXPECT_EQ(connectedCalled,1);
}


TEST_F(WifiClientTest, ConnectWhenConnected){
     EXPECT_TRUE(client->connect());
     EXPECT_FALSE(client->connect());
     EXPECT_TRUE(client->isConnected());
}

TEST_F(WifiClientTest, Disconnect){
    int disconnectedCalled = 0;
    client->connect();
    EXPECT_TRUE(client->isConnected());
    client->disconnect();
    EXPECT_FALSE(client->isConnected());

}

TEST_F(WifiClientTest, DisconnectWithCallback){
    int disconnectedCalled = 0;
    client->setOnDisconnectedCallback([&disconnectedCalled](){disconnectedCalled++;});
    client->connect();
    client->disconnect();
    EXPECT_EQ(disconnectedCalled,1);

}


TEST_F(WifiClientTest, WifiClientNotSucceededTimeout) {
    using namespace std::chrono_literals;
    timer->enable(true);
    int connectionFailedCalled = 0;
    client->setOnNotSucceededTimeoutCallback([&connectionFailedCalled](){connectionFailedCalled++;});
    client->setNotSucceededTimeout(30s);
    client->holdConnection(true);
    EXPECT_FALSE(client->connect());
    EXPECT_FALSE(client->isConnected());
    EXPECT_EQ(connectionFailedCalled,0);
    timer->tick(29999);
    EXPECT_FALSE(client->isConnected());
    EXPECT_EQ(connectionFailedCalled,0);
    timer->tick(100);
    EXPECT_FALSE(client->isConnected());
    EXPECT_EQ(connectionFailedCalled,1);
}