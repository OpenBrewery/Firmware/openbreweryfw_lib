//
// Created by MICHAL on 21.10.2018.
//

#ifndef OPENBREWERYFW_LIB_DUMMYTRANSACTIONINTERFACE_H
#define OPENBREWERYFW_LIB_DUMMYTRANSACTIONINTERFACE_H

#include <TransactionInterface.h>
#include "DummyCommInterface.h"
#include <vector>

namespace Peripheral {
    class DummyTransactionInterface : virtual public TransactionInterface<uint8_t>, virtual public DummyCommInterface {
    public:
        std::vector<uint8_t> Response;
        std::vector<uint8_t> Request;


    protected:
        bool __write(etl::array_view<uint8_t> data) override;


        size_t __request(etl::array_view<uint8_t> request, etl::ivector<uint8_t> &response, size_t max_response_len) override;

    };
}

#endif //OPENBREWERYFW_LIB_DUMMYTRANSACTIONINTERFACE_H
