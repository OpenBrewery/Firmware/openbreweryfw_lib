//
// Created by mchodzikiewicz on 10/6/18.
//

#ifndef OPENBREWERYFW_LIB_DUMMYINTERFACE_H
#define OPENBREWERYFW_LIB_DUMMYINTERFACE_H

#include <vector>
#include "AsyncInterface.h"

namespace Peripheral {
    class DummyAsyncInterface : virtual public AsyncInterface<uint8_t> {
    public:
        DummyAsyncInterface() : AsyncInterface() {};

        bool open() override;
        void close() override;
        bool isOpened() const override __attribute__((pure));

        size_t __write(const etl::ivector<uint8_t> &packet) override;

        size_t __write(const uint8_t *buffer, size_t len) override;

        size_t __read(etl::ivector<uint8_t> &buffer, size_t max_length) override;
        size_t __read(uint8_t * buffer, size_t max_length) override;

        size_t bytesAvailable(void) const override;

//        bool waitForBytesWritten(std::chrono::milliseconds msecs) override;
//
//        bool waitForReadyRead(std::chrono::milliseconds) override;


        void call_readyRead();


        std::vector<uint8_t> writeBuffer;
        std::vector<uint8_t> readBuffer;
        bool Opened = false;
    };
}


#endif //OPENBREWERYFW_LIB_DUMMYINTERFACE_H
