//
// Created by mchodzikiewicz on 10/11/18.
//

#include "SerialInterface.h"
#include "gtest/gtest.h"
#include "DummyAsyncInterface.h"

using namespace Peripheral;

class DummySerialInterface : public SerialInterface, public DummyAsyncInterface {
public:
    explicit DummySerialInterface() : SerialInterface(
            9600,
            SerialInterface::Config::Parity::None,
            SerialInterface::Config::StopBits::One,
            SerialInterface::Config::DataBits::Data8,
            SerialInterface::Config::FlowControl::NoFlowControl,
            SerialInterface::Config::Mode::ReadWrite
            ) , DummyAsyncInterface() {}

    bool setConfig(
        uint32_t baudrate,
        Config::Parity parity,
        Config::StopBits stopBits,
        Config::DataBits dataBits,
        Config::FlowControl flowControl,
        Config::Mode mode) override
    {
        Config.baudrate = baudrate;
        Config.parity = parity;
        Config.stopBits = stopBits;
        Config.dataBits = dataBits;
        Config.flowControl = flowControl;
        Config.mode = mode;
        return true;
    }

};

class SerialInterfaceTest : public ::testing::Test {
public:
    void SetUp() override {
        errors_triggered = 0;
        interface = std::make_unique<DummySerialInterface>();
        interface->setErrorCallback([this](){errors_triggered++;});
    };
    void TearDown() override {
        if(errors_triggered){
            int errs = errors_triggered;
            errors_triggered = 0;
            FAIL() << "Unexpected errors: " << errs;
        }
    };

    std::unique_ptr<SerialInterface> interface;
    int errors_triggered;
};

TEST_F(SerialInterfaceTest, CheckConstructorConfig) {
    EXPECT_FALSE(interface->isOpened());
    EXPECT_EQ(interface->getConfig().baudrate,9600);
    EXPECT_EQ(interface->getConfig().parity,SerialInterface::Config::Parity::None);
    EXPECT_EQ(interface->getConfig().stopBits,SerialInterface::Config::StopBits::One);
    EXPECT_EQ(interface->getConfig().dataBits,SerialInterface::Config::DataBits::Data8);
    EXPECT_EQ(interface->getConfig().flowControl,SerialInterface::Config::FlowControl::NoFlowControl);
    EXPECT_EQ(interface->getConfig().mode,SerialInterface::Config::Mode::ReadWrite);
}

TEST_F(SerialInterfaceTest, ChangeConfig) {
    EXPECT_TRUE(interface->setConfig(
            115200,
            SerialInterface::Config::Parity::Odd,
            SerialInterface::Config::StopBits::Two,
            SerialInterface::Config::DataBits::Data6,
            SerialInterface::Config::FlowControl::HardwareControl,
            SerialInterface::Config::Mode::HalfDuplex
    ));
    EXPECT_EQ(interface->getConfig().baudrate,115200);
    EXPECT_EQ(interface->getConfig().parity,SerialInterface::Config::Parity::Odd);
    EXPECT_EQ(interface->getConfig().stopBits,SerialInterface::Config::StopBits::Two);
    EXPECT_EQ(interface->getConfig().dataBits,SerialInterface::Config::DataBits::Data6);
    EXPECT_EQ(interface->getConfig().flowControl,SerialInterface::Config::FlowControl::HardwareControl);
    EXPECT_EQ(interface->getConfig().mode,SerialInterface::Config::Mode::HalfDuplex);
}

TEST_F(SerialInterfaceTest, Open){
    EXPECT_FALSE(interface->isOpened());
    EXPECT_TRUE(interface->open());
    EXPECT_TRUE(interface->isOpened());
}