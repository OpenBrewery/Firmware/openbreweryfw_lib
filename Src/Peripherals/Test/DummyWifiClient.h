//
// Created by mchodzikiewicz on 2/2/19.
//

#ifndef OPENBREWERYFW_LIB_DUMMYWIFICLIENT_H
#define OPENBREWERYFW_LIB_DUMMYWIFICLIENT_H

#include <optional>
#include "WifiClient.h"

namespace Peripheral {
    class DummyWifiClient : public WifiClient {
    public:

        explicit DummyWifiClient(etl::imessage_timer & timer) : WifiClient(timer), Connected(false), Initialized(false), ConnectionOnHold(false), TryingToConnect(false) {}

        bool init() override;

        bool setSSID(etl::string_view ssid) override;

        bool setPassword(etl::string_view passwd) override;

        bool connect() override;

        bool isConnected() override;

        etl::string_view getSSID() override;

        etl::string_view getPassword() override;

        std::optional<etl::string_view> getIP(etl::istring & ip_buf) override;

        void disconnect() override;


        void holdConnection(bool hold);


    private:
        std::string SSID;
        std::string Passwd;
        bool Initialized;
        bool Connected;
        //TODO (mchodzikiewicz): STATE MACHINE IS THE ONLY VIABLE SOLUTION HERE :(
        bool TryingToConnect;

        bool ConnectionOnHold;
    };
}


#endif //OPENBREWERYFW_LIB_DUMMYWIFICLIENT_H
