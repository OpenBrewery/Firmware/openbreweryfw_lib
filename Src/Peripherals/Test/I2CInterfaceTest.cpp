//
// Created by mchodzikiewicz on 10/7/18.
//

#include "gtest/gtest.h"
#include "DummyI2CInterface.h"

using namespace Peripheral;



class I2CInterfaceTest : public ::testing::Test {
public:
    void SetUp() override {
        errors_triggered = 0;
        interface = std::make_unique<DummyI2CInterface>();
        interface->setErrorCallback([this](){errors_triggered++;});
    };
    void TearDown() override {
        if(errors_triggered){
            int errs = errors_triggered;
            errors_triggered = 0;
            FAIL() << "Unexpected errors: " << errs;
        }
    };

    std::unique_ptr<I2CInterface> interface;
    int errors_triggered;
};

TEST_F(I2CInterfaceTest, CheckConstructorConfig) {
    EXPECT_EQ(interface->getConfig().mode,I2CInterface::Config::Mode::Master);
    EXPECT_EQ(interface->getConfig().Frequency,100000);
}

TEST_F(I2CInterfaceTest, ChangeConfig) {
    EXPECT_TRUE(interface->setConfig(I2CInterface::Config::Mode::Slave,500));
    EXPECT_EQ(interface->getConfig().mode,I2CInterface::Config::Mode::Slave);
    EXPECT_EQ(interface->getConfig().Frequency,500);
}

TEST_F(I2CInterfaceTest, Open){
    EXPECT_FALSE(interface->isOpened());
    EXPECT_TRUE(interface->open());
    EXPECT_TRUE(interface->isOpened());
}
