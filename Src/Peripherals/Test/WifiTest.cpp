//
// Created by mchodzikiewicz on 2/13/19.
//

#include "gtest/gtest.h"
#include "Wifi.h"
#include "DummyWifiClient.h"
#include "DummyWifiServer.h"

using namespace Peripheral;

class WifiTest : public ::testing::Test {
public:
    void SetUp() {
        timer = new etl::message_timer<1>;
        client = new DummyWifiClient(*timer);
        server = new DummyWifiServer(*timer);
    };
    void TearDown() {

    };

    WifiClient * client;
    WifiServer * server;
    etl::imessage_timer * timer;
};

TEST_F(WifiTest, Init) {

    Wifi manager(std::move(*client),std::move(*server));
    EXPECT_FALSE(manager.isInUse());
}

TEST_F(WifiTest, GetClient) {
    Wifi manager(std::move(*client),std::move(*server));
    std::unique_ptr<WifiClient> retrievedClient = manager.getClient();
    EXPECT_EQ(static_cast<WifiClient*>(client),retrievedClient.get());
    EXPECT_TRUE(manager.isInUse());
    EXPECT_EQ(nullptr,manager.getClient());
    EXPECT_EQ(nullptr,manager.getServer());
    retrievedClient.release();
}

TEST_F(WifiTest, GetServer) {
    Wifi manager(std::move(*client),std::move(*server));
    std::unique_ptr<WifiServer> retrievedServer = manager.getServer();
    EXPECT_EQ(static_cast<WifiServer*>(server),retrievedServer.get());
    EXPECT_TRUE(manager.isInUse());
    EXPECT_EQ(nullptr,manager.getClient());
    EXPECT_EQ(nullptr,manager.getServer());
    retrievedServer.release();
}

TEST_F(WifiTest, ReturnClient) {
    Wifi manager(std::move(*client),std::move(*server));
    auto Client = manager.getClient();
    EXPECT_TRUE(manager.isInUse());
    manager.returnClient(std::move(Client));
    EXPECT_EQ(Client,nullptr);
    EXPECT_FALSE(manager.isInUse());
    std::unique_ptr<WifiClient> retrievedClient = manager.getClient();
    EXPECT_EQ(static_cast<WifiClient*>(client),retrievedClient.get());
    EXPECT_TRUE(manager.isInUse());
    EXPECT_EQ(nullptr,manager.getServer());
    retrievedClient.release();

}

TEST_F(WifiTest, ReturnServer) {
    Wifi manager(std::move(*client),std::move(*server));
    auto Server = std::move(manager.getServer());
    EXPECT_TRUE(manager.isInUse());
    manager.returnServer(std::move(Server));
    EXPECT_EQ(Server,nullptr);
    EXPECT_FALSE(manager.isInUse());
    std::unique_ptr<WifiClient> retrievedClient = manager.getClient();
    EXPECT_EQ(static_cast<WifiClient*>(client),retrievedClient.get());
    EXPECT_TRUE(manager.isInUse());
    EXPECT_EQ(nullptr,manager.getServer());
    retrievedClient.release();
}