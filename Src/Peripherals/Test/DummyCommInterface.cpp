//
// Created by michal on 24.10.18.
//

#include "DummyCommInterface.h"

bool Peripheral::DummyCommInterface::open() {
    if (Opened){
        emit_errorCallback();
        return false;
    }
    Opened = true;
    emit_connectedCallback();
    return Opened;
}

void Peripheral::DummyCommInterface::close() {
    Opened = false;
}

bool Peripheral::DummyCommInterface::isOpened() const {
    return Opened;
}
