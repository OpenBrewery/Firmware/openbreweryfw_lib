//
// Created by mchodzikiewicz on 2/13/19.
//

#ifndef OPENBREWERYFW_LIB_DUMMYWIFISERVER_H
#define OPENBREWERYFW_LIB_DUMMYWIFISERVER_H

#include <WifiServer.h>

namespace Peripheral {
    class DummyWifiServer : public WifiServer {
    public:
        DummyWifiServer(etl::imessage_timer & timer) : IsServing(false), WifiServer(timer) {}
        bool init() override;

        bool setSSID(etl::string_view ssid) override;

        bool setPassword(etl::string_view passwd) override;

        etl::string_view getSSID() override;

        etl::string_view getPassword() override;

        bool serve() override;

        void stop() override;

        bool isServing() override;

    private:
        bool IsServing;
        std::string SSID;
        std::string Password;
    };
}

#endif //OPENBREWERYFW_LIB_DUMMYWIFISERVER_H
