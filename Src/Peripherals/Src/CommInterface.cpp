//
// Created by MICHAL on 21.10.2018.
//

#include "CommInterface.h"

using namespace Peripheral;

void CommInterface::setErrorCallback(const std::function<void(void)> callback) {
    errorCallback = callback;
}

void CommInterface::setConnectedCallback(const std::function<void(void)> callback) {
    connectedCallback = callback;
}

void CommInterface::setDisconnectedCallback(const std::function<void(void)> callback) {
    disconnectedCallback = callback;
}

void CommInterface::emit_errorCallback() {
    if(errorCallback) errorCallback();
}

void CommInterface::emit_connectedCallback() {
    if(connectedCallback) connectedCallback();
}

void CommInterface::emit_disconnectedCallback() {
    if(disconnectedCallback) disconnectedCallback();
}