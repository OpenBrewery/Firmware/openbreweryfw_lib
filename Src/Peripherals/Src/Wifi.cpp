//
// Created by mchodzikiewicz on 2/13/19.
//

#include <Wifi.h>

using namespace Peripheral;

std::unique_ptr<WifiClient> Wifi::getClient() {
    if(!InUse) {
        InUse = true;
        return std::unique_ptr<WifiClient>(&Client);
    }
     return nullptr;
}

std::unique_ptr<WifiServer> Peripheral::Wifi::getServer() {
    if(!InUse) {
        InUse = true;
        return std::unique_ptr<WifiServer>(&Server);
    }
    return nullptr;
}

bool Wifi::isInUse() {
    return InUse;
}

void Wifi::returnClient(std::unique_ptr<Peripheral::WifiClient> client) {
    if(client.get() == &Client) {
        InUse = false;
    }
}

void Wifi::returnServer(std::unique_ptr<Peripheral::WifiServer> server) {
    if(server.get() == &Server) {
        InUse = false;
    }
}

