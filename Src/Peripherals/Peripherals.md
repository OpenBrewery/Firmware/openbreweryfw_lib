# Peripherals

This module contains commonly used peripherals, such as I2C, WiFi, USART, Digital/Analog GPIO


## Communication peripherals abstraction
If this is possible, all communication peripherals should inherit either:
### AsyncInterface
If there is no direct correlation between reading and writing, communication is asynchronous, which means that you can write/read at any time.

examples:
* UART
* TCP/UDP (from interface perspective) 

### TransactionInterace
If there are transactions (ie. Master always initiates transmission and slave can only reply on demand)

examples:
* I2C
* SPI
* HTTP