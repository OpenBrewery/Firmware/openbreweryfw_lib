//
// Created by mchodzikiewicz on 8/5/19.
//

#ifndef OPENBREWERYFW_LIB_FIRMWAREMANAGER_H
#define OPENBREWERYFW_LIB_FIRMWAREMANAGER_H

#include <etl/string_view.h>

namespace System {
    class FirmwareManager {
    public:

        virtual bool downloadFirmwarePackage(etl::string_view url) = 0;
    };
}

#endif //OPENBREWERYFW_LIB_FIRMWAREMANAGER_H
