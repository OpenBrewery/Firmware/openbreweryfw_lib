//
// Created by mchodzikiewicz on 3/2/19.
//

#ifndef OPENBREWERYFW_LIB_FILE_H
#define OPENBREWERYFW_LIB_FILE_H

#include <etl/cstring.h>
#include <etl/string_view.h>
#include <etl/vector.h>
#include <etl/variant.h>
#include <etl/array_view.h>
#include <optional>
#include <chrono>

namespace System {
    class Registry {
    public:
        using StoredValue = etl::variant<uint8_t, int, float, bool, etl::string<32>,std::chrono::milliseconds, std::chrono::time_point<std::chrono::system_clock>>;
        virtual std::optional<StoredValue> getEntry(etl::string_view entry) = 0;
        virtual bool storeEntry(etl::string_view entry, StoredValue value) = 0;
        virtual bool eraseEntry(etl::string_view entry) = 0;

        virtual bool eraseRegistry() = 0;
    };
}

#endif //OPENBREWERYFW_LIB_FILE_H
