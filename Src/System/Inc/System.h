//
// Created by mchodzikiewicz on 3/2/19.
//

#ifndef OPENBREWERYFW_LIB_DEVICEID_H
#define OPENBREWERYFW_LIB_DEVICEID_H

#include <etl/string_view.h>
#include <chrono>
#include <optional>

namespace System {
    class System {
    public:
        using Version = etl::string_view;

        virtual etl::string_view getDeviceId() = 0;
        virtual void reboot() = 0;
        virtual void deepSleep() = 0;
        virtual void deepSleep(std::chrono::milliseconds sleepTime) = 0;

        virtual std::chrono::time_point<std::chrono::system_clock> now() = 0;
        virtual void setClock(std::chrono::time_point<std::chrono::system_clock> timePoint) = 0;

        virtual void delay(std::chrono::milliseconds time) = 0;

        virtual void syncClock() = 0;

        //TODO (mchodzikiewicz): battery should be represented by separate object
        virtual float batteryVoltage() = 0;

        virtual std::chrono::milliseconds uptime() = 0;
        virtual std::optional<std::chrono::milliseconds> previousUptime() = 0;


        virtual Version version() = 0;
    };
}


#endif //OPENBREWERYFW_LIB_DEVICEID_H
