//
// Created by mchodzikiewicz on 8/5/19.
//

#ifndef OPENBREWERYFW_LIB_DUMMYFIRMWAREMANAGER_H
#define OPENBREWERYFW_LIB_DUMMYFIRMWAREMANAGER_H

#include "FirmwareManager.h"

namespace System {
    class DummyFirmwareManager : public FirmwareManager {
    public:
        bool downloadFirmwarePackage(etl::string_view url) override;

        bool Result = true;
        std::string Url;
    };

}


#endif //OPENBREWERYFW_LIB_DUMMYFIRMWAREMANAGER_H
