//
// Created by mchodzikiewicz on 3/2/19.
//

#ifndef OPENBREWERYFW_LIB_DUMMYREGISTRY_H
#define OPENBREWERYFW_LIB_DUMMYREGISTRY_H

#include <Registry.h>
#include <map>

namespace System {
    class DummyRegistry : public Registry {
    public:
        std::optional<StoredValue> getEntry(etl::string_view entry) override;

        bool storeEntry(etl::string_view entry, StoredValue value) override;

        bool eraseEntry(etl::string_view entry) override;

        using Registry::storeEntry;

        bool eraseRegistry() override;

        std::map<std::string, StoredValue> Map;
    };
}


#endif //OPENBREWERYFW_LIB_DUMMYREGISTRY_H
