//
// Created by mchodzikiewicz on 8/5/19.
//

#include "gtest/gtest.h"
#include "DummyFirmwareManager.h"


class DummyFirmwareMangerTest : public ::testing::Test {
    void SetUp() override {};
    void TearDown() override {};

};


TEST_F(DummyFirmwareMangerTest,Init){
    System::DummyFirmwareManager firmwareManager;
    EXPECT_TRUE(firmwareManager.downloadFirmwarePackage("testUrl"));
    EXPECT_STREQ(firmwareManager.Url.c_str(),"testUrl");
}