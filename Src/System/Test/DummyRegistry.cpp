//
// Created by mchodzikiewicz on 3/2/19.
//

#include "DummyRegistry.h"

std::optional<System::Registry::StoredValue> System::DummyRegistry::getEntry(etl::string_view entry) {
    auto element = Map.find(std::string(entry.cbegin(),entry.cend()));
    if(element != Map.cend())
        return element->second;
    else return std::nullopt;
}

bool System::DummyRegistry::storeEntry(etl::string_view entry, System::Registry::StoredValue value) {
    std::string entryStr(entry.cbegin(),entry.cend());
    Map.erase(entryStr);
    Map.insert({entryStr,value});
    return true;
}

bool System::DummyRegistry::eraseRegistry() {
    Map.clear();
    return true;
}

bool System::DummyRegistry::eraseEntry(etl::string_view entry) {
    std::string entryStr(entry.cbegin(),entry.cend());
    if(Map.erase(entryStr))
        return true;
    else
        return false;
}
