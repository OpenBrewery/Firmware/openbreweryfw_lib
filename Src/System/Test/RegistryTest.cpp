//
// Created by mchodzikiewicz on 3/2/19.
//

#include <etl/cstring.h>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "DummyRegistry.h"

using testing::ElementsAre;

class RegistryTest : public ::testing::Test {
    void SetUp() override {};
    void TearDown() override {};

};


TEST_F(RegistryTest,Init){
    System::DummyRegistry registry;
    EXPECT_EQ(registry.Map.size(),0);
}

TEST_F(RegistryTest,AddEntry_u8){
    System::DummyRegistry registry;
    EXPECT_EQ(registry.storeEntry("TEST1",static_cast<uint8_t>(180)),true);
    EXPECT_EQ(registry.getEntry("TEST1").value().get<uint8_t>(),180);
}

TEST_F(RegistryTest,AddEntry_float){
    System::DummyRegistry registry;
    EXPECT_EQ(registry.storeEntry("TEST1",static_cast<float>(0.123)),true);
    EXPECT_FLOAT_EQ(registry.getEntry("TEST1").value().get<float>(),0.123);
}

TEST_F(RegistryTest,AddEntry_duration){
    using namespace std::chrono_literals;
    System::DummyRegistry registry;
    EXPECT_EQ(registry.storeEntry("TEST1",std::chrono::duration_cast<std::chrono::milliseconds>(5s)),true);
    EXPECT_EQ(registry.getEntry("TEST1").value().get<std::chrono::milliseconds>(),5s);

    EXPECT_EQ(registry.storeEntry("TEST2",50ms),true);
    EXPECT_EQ(registry.getEntry("TEST2").value().get<std::chrono::milliseconds>(),50ms);

    EXPECT_EQ(registry.storeEntry("TEST3",std::chrono::duration_cast<std::chrono::milliseconds>(123h)),true);
    EXPECT_EQ(registry.getEntry("TEST3").value().get<std::chrono::milliseconds>(),123h);

}

TEST_F(RegistryTest,AddTwoEntries){
    System::DummyRegistry registry;
    EXPECT_EQ(registry.storeEntry("TEST1",static_cast<uint8_t>(180)),true);
    EXPECT_EQ(registry.storeEntry("VAR2",static_cast<float>(180.01)),true);
    EXPECT_EQ(registry.getEntry("TEST1").value().get<uint8_t>(),180);
    EXPECT_FLOAT_EQ(registry.getEntry("VAR2").value().get<float>(),180.01);
}

TEST_F(RegistryTest,ReadWrongType){
    System::DummyRegistry registry;
    EXPECT_EQ(registry.storeEntry("TEST1",static_cast<float>(0.123)),true);
    EXPECT_THROW(registry.getEntry("TEST1").value().get<int>(),etl::variant_incorrect_type_exception);
}

TEST_F(RegistryTest,ReadNonExistingRes){
    System::DummyRegistry registry;
    EXPECT_EQ(registry.getEntry("TEST1"),std::nullopt);
}

TEST_F(RegistryTest,EraseRegistry){
    System::DummyRegistry registry;
    EXPECT_EQ(registry.storeEntry("TEST1",static_cast<float>(0.123)),true);
    EXPECT_FLOAT_EQ(registry.getEntry("TEST1").value().get<float>(),0.123);
    EXPECT_TRUE(registry.eraseRegistry());
    EXPECT_EQ(registry.getEntry("TEST1"),std::nullopt);

}

TEST_F(RegistryTest,EraseEntry){
    System::DummyRegistry registry;
    EXPECT_EQ(registry.storeEntry("TEST1",static_cast<uint8_t>(180)),true);
    EXPECT_EQ(registry.storeEntry("VAR2",static_cast<float>(180.01)),true);
    EXPECT_EQ(registry.getEntry("TEST1").value().get<uint8_t>(),180);
    EXPECT_FLOAT_EQ(registry.getEntry("VAR2").value().get<float>(),180.01);
    EXPECT_FALSE(registry.eraseEntry("NONEXISTINGENTRY"));
    EXPECT_TRUE(registry.eraseEntry("VAR2"));
    EXPECT_EQ(registry.getEntry("TEST1").value().get<uint8_t>(),180);
    EXPECT_FALSE(registry.getEntry("VAR2"));
}
