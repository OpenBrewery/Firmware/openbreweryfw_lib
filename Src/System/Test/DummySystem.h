//
// Created by mchodzikiewicz on 3/2/19.
//

#ifndef OPENBREWERYFW_LIB_DUMMYDEVICEID_H
#define OPENBREWERYFW_LIB_DUMMYDEVICEID_H

#include <System.h>

namespace System {
    class DummySystem : public System {
    public:
        DummySystem() : rebootCounter(0)
                        , deepSleepCounter(0)
                        , sleepTime(std::chrono::milliseconds(0))
                        , timeSyncSuccessful(true)
                        , batV(4.0)
                        , Uptime(std::chrono::milliseconds(1000))
                        , PrevUptime(std::chrono::milliseconds(1000))
                        , Ver{""}
                        {}

        etl::string_view getDeviceId() override;

        bool setDeviceId(etl::string_view id);

        void reboot() override;

        void deepSleep() override;

        void deepSleep(std::chrono::milliseconds sleepTime) override;

        std::chrono::time_point<std::chrono::system_clock> now() override;

        void delay(std::chrono::milliseconds time) override;

        std::chrono::milliseconds uptime() override;

        std::optional<std::chrono::milliseconds> previousUptime() override;


        void syncClock() override;

        void setClock(std::chrono::time_point<std::chrono::system_clock> timePoint) override;

        float batteryVoltage() override;

        Version version() override;

        std::string Id;

        size_t rebootCounter;
        size_t deepSleepCounter;
        std::chrono::milliseconds sleepTime;

        std::chrono::time_point<std::chrono::system_clock> currentTimestamp;
        std::vector<std::chrono::milliseconds> delayCalls;

        bool timeSyncSuccessful;

        float batV;

        std::chrono::milliseconds Uptime;
        std::optional<std::chrono::milliseconds> PrevUptime;

        Version Ver;

    };
}

#endif //OPENBREWERYFW_LIB_DUMMYDEVICEID_H
