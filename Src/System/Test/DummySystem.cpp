//
// Created by mchodzikiewicz on 3/2/19.
//

#include "DummySystem.h"
#include <optional>

etl::string_view System::DummySystem::getDeviceId() {
    return etl::string_view(Id.c_str(),Id.size());
}


bool System::DummySystem::setDeviceId(etl::string_view id){
    Id.assign(id.cbegin(),id.cend());
    return true;
}

void System::DummySystem::reboot() {
    rebootCounter++;
}

void System::DummySystem::deepSleep() {
    deepSleepCounter++;
    throw std::nullopt;
}

void System::DummySystem::deepSleep(std::chrono::milliseconds time) {
    deepSleepCounter++;
    sleepTime = time;
    throw sleepTime;
}

std::chrono::time_point<std::chrono::system_clock> System::DummySystem::now() {
    return currentTimestamp;
}

void System::DummySystem::setClock(std::chrono::time_point<std::chrono::system_clock> timePoint) {
    currentTimestamp = timePoint;
}

void System::DummySystem::syncClock() {

}

float System::DummySystem::batteryVoltage() {
    return batV;
}

std::chrono::milliseconds System::DummySystem::uptime() {
    return Uptime;
}

std::optional<std::chrono::milliseconds> System::DummySystem::previousUptime() {
    return PrevUptime;
}

System::System::Version System::DummySystem::version() {
    return Ver;
}

void System::DummySystem::delay(std::chrono::milliseconds time) {
    delayCalls.push_back(time);
}

