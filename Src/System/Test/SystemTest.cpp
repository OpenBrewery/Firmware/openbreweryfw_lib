//
// Created by mchodzikiewicz on 3/2/19.
//

#include "gtest/gtest.h"
#include "DummySystem.h"
#include <optional>

class SystemTest : public ::testing::Test {
    void SetUp() override {};
    void TearDown() override {};

};


TEST_F(SystemTest,Init){
    System::DummySystem system;
    EXPECT_STREQ("",system.getDeviceId().cbegin());
}

TEST_F(SystemTest,SetId){
    System::DummySystem system;
    system.setDeviceId("1234");
    EXPECT_STREQ("1234",system.getDeviceId().cbegin());
}

TEST_F(SystemTest,Reboot){
    System::DummySystem system;
    EXPECT_EQ(system.rebootCounter,0);
    system.reboot();
    EXPECT_EQ(system.rebootCounter,1);
}

TEST_F(SystemTest,DeepSleep){
    System::DummySystem system;
    EXPECT_EQ(system.deepSleepCounter,0);
    EXPECT_THROW(system.deepSleep(),std::nullopt_t);
    EXPECT_EQ(system.deepSleepCounter,1);
}

TEST_F(SystemTest,DeepSleepForMs){
    using namespace std::chrono_literals;
    System::DummySystem system;
    EXPECT_EQ(system.deepSleepCounter,0);
    EXPECT_THROW(system.deepSleep(150ms),std::chrono::milliseconds);
    EXPECT_EQ(system.deepSleepCounter,1);
    EXPECT_EQ(system.sleepTime,150ms);
    EXPECT_THROW(system.deepSleep(12345ms),std::chrono::milliseconds);
    EXPECT_EQ(system.deepSleepCounter,2);
    EXPECT_EQ(system.sleepTime,12345ms);
}

TEST_F(SystemTest,Now){
    using namespace std::chrono_literals;
    System::DummySystem system;
    EXPECT_EQ(system.now(),std::chrono::system_clock::from_time_t(0));
    tm time;
    time.tm_hour = 10;
    time.tm_min = 50;
    time.tm_sec = 15;
    time.tm_year = 109;
    time.tm_mon = 4;
    time.tm_mday = 3;
    system.currentTimestamp = std::chrono::system_clock::from_time_t(mktime(&time));
    EXPECT_EQ(system.now(),std::chrono::system_clock::from_time_t(mktime(&time)));
}

TEST_F(SystemTest,SetTime){
    using namespace std::chrono_literals;
    System::DummySystem system;
    EXPECT_EQ(system.now(),std::chrono::system_clock::from_time_t(0));
    tm time;
    time.tm_hour = 10;
    time.tm_min = 50;
    time.tm_sec = 15;
    time.tm_year = 109;
    time.tm_mon = 4;
    time.tm_mday = 3;
    system.setClock(std::chrono::system_clock::from_time_t(mktime(&time)));
    EXPECT_EQ(system.now(),std::chrono::system_clock::from_time_t(mktime(&time)));
}


TEST_F(SystemTest,BatteryVoltage) {
    System::DummySystem system;
    EXPECT_FLOAT_EQ(system.batteryVoltage(), 4.0);
    system.batV = 4.3;
    EXPECT_FLOAT_EQ(system.batteryVoltage(), 4.3);
}


TEST_F(SystemTest,Uptime){
    using namespace std::chrono_literals;
    System::DummySystem system;
    EXPECT_EQ(system.uptime(), 1000ms);
    system.Uptime = 1234ms;
    EXPECT_EQ(system.uptime(), 1234ms);
}

TEST_F(SystemTest,PreviousUptime){
    using namespace std::chrono_literals;
    System::DummySystem system;
    EXPECT_EQ(system.previousUptime(), 1000ms);
    system.PrevUptime = 1234ms;
    EXPECT_EQ(system.previousUptime().value(), 1234ms);
}


TEST_F(SystemTest,Version){
    System::DummySystem system;
    System::System::Version version = system.version();
    EXPECT_STREQ(version.data(),"");
    system.Ver = "Test description";
    version = system.version();
    EXPECT_STREQ(version.data(),"Test description");
}

TEST_F(SystemTest,Delay){
    using namespace std::chrono_literals;
    System::DummySystem system;
    EXPECT_EQ(system.delayCalls.size(), 0);
    system.delay(100s);
    EXPECT_EQ(system.delayCalls.size(), 1);
    EXPECT_EQ(system.delayCalls.at(0),100s);
    system.delay(3ms);
    EXPECT_EQ(system.delayCalls.size(), 2);
    EXPECT_EQ(system.delayCalls.at(0),100s);
    EXPECT_EQ(system.delayCalls.at(1),3ms);
}

