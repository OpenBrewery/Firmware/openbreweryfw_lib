# HTTP SERVER 

HttpServer class is designed to make dynamic http response content creation.
Since it is trying to avoid dynamic allocation of memory, each request needs to get a buffer to write into