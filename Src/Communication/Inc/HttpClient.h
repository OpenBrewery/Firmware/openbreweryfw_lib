//
// Created by mchodzikiewicz on 13.12.18.
//

#ifndef OPENBREWERYFW_LIB_HTTPSOCKET_H
#define OPENBREWERYFW_LIB_HTTPSOCKET_H

#include "etl/string_view.h"
#include <optional>
#include <etl/cstring.h>
#include "HttpResponse.h"


namespace Communication {
    class HttpClient {
    public:
        virtual HttpResponse get(etl::istring & resBuffer, etl::string_view host, int port, etl::string_view target) = 0;
        virtual HttpResponse post(etl::istring & reqBuffer, etl::istring & resBuffer, etl::string_view host, int port, etl::string_view target, etl::string_view content) = 0;

        virtual HttpResponse Sget(etl::istring & resBuffer, etl::string_view host, int port, etl::string_view target) = 0;
        virtual HttpResponse Spost(etl::istring & reqBuffer, etl::istring & resBuffer, etl::string_view host, int port, etl::string_view target, etl::string_view content) = 0;



        virtual void setAuthorization(etl::string_view auth_str) = 0;

    };

}


#endif //OPENBREWERYFW_LIB_HTTPSOCKET_H
