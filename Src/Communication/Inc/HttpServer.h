//
// Created by mchodzikiewicz on 15.12.18.
//

#ifndef OPENBREWERY_BSP_LINUX_HTTPSERVER_H
#define OPENBREWERY_BSP_LINUX_HTTPSERVER_H


#include <etl/string_view.h>
#include <etl/cstring.h>
#include <HttpResponse.h>

namespace Communication {
    class HttpServer {
    public:
        enum class ResourceType {Post,Get};

        virtual bool serve(etl::string_view host, uint16_t port) = 0;

        //TODO (mchodzikiewicz): this kind of interface is basically not safe thread-wise, easy to create mem leaks etc... we should pass ownership of response strings somehow...
        virtual bool addGetResource(etl::string_view path, std::function<HttpResponse(etl::istring&)> function) = 0;
        virtual bool addPostResource(etl::string_view path, std::function<HttpResponse(etl::istring&, etl::string_view)> function) = 0;

        virtual bool removeGetResource(etl::string_view path) = 0;
        virtual bool removePostResource(etl::string_view path) = 0;

        virtual void stop() = 0;

        virtual bool isServing() = 0;

    };
}


#endif //OPENBREWERY_BSP_LINUX_HTTPSERVER_H
