//
// Created by mchodzikiewicz on 3/6/19.
//

#ifndef OPENBREWERYFW_LIB_HTTPRESPONSE_H
#define OPENBREWERYFW_LIB_HTTPRESPONSE_H

#include <optional>

namespace Communication {
    struct HttpResponse {
        uint16_t ResCode;
        std::optional<etl::string_view> Response;
    };
}

#endif //OPENBREWERYFW_LIB_HTTPRESPONSE_H
