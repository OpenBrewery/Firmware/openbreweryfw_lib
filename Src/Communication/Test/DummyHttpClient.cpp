//
// Created by mchodzikiewicz on 15.12.18.
//

#include "DummyHttpClient.h"


Communication::HttpResponse Communication::DummyHttpClient::get(etl::istring & resBuffer, etl::string_view host, int port, etl::string_view target) {
    return {ResCode,etl::string_view(injectedResponse.c_str(),injectedResponse.size())};
}

Communication::HttpResponse Communication::DummyHttpClient::post(etl::istring &reqBuffer, etl::istring & resBuffer, etl::string_view host, int port, etl::string_view target, etl::string_view content) {
    lastRequest.assign(content.cbegin(),content.cend());
    return {ResCode,etl::string_view(injectedResponse.c_str(),injectedResponse.size())};

}

void Communication::DummyHttpClient::setAuthorization(etl::string_view auth_str) {
    Authorization.assign(auth_str.cbegin(),auth_str.cend());
}

Communication::HttpResponse
Communication::DummyHttpClient::Sget(etl::istring &resBuffer, etl::string_view host, int port,
                                     etl::string_view target) {
    return {ResCode,etl::string_view(injectedResponse.c_str(),injectedResponse.size())};
}

Communication::HttpResponse
Communication::DummyHttpClient::Spost(etl::istring &reqBuffer, etl::istring &resBuffer, etl::string_view host, int port, etl::string_view target,
                                      etl::string_view content) {
    lastRequest.assign(content.cbegin(),content.cend());
    return {ResCode,etl::string_view(injectedResponse.c_str(),injectedResponse.size())};

}

