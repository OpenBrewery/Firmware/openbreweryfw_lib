//
// Created by mchodzikiewicz on 2/11/19.
//

#include "gtest/gtest.h"
#include "DummyHttpClient.h"

using namespace Communication;

class DummyHttpServerTest : public ::testing::Test {
public:
    void SetUp() {
        httpServer = new DummyHttpServer;

    };

    void TearDown() {
        delete httpServer;
    };

    DummyHttpServer * httpServer;


};



TEST_F(DummyHttpServerTest,addGetResource){
    EXPECT_TRUE(httpServer->addGetResource("TEST",[](etl::istring & resBuffer) -> HttpResponse {
        resBuffer.clear();
        resBuffer.assign("Super Test");
        return {200,etl::string_view(resBuffer.c_str(),resBuffer.size())};
    }));
    etl::string<100> resBuf;
    etl::string<100> reqBuf;
    HttpResponse response = httpServer->get(resBuf,"",0,"TEST");
    EXPECT_STREQ("Super Test",response.Response.value().cbegin());
}

TEST_F(DummyHttpServerTest,getNonExists){
    etl::string<100> resBuf;
    etl::string<100> reqBuf;
    HttpResponse response = httpServer->get(resBuf,"",0,"TEST");
    EXPECT_EQ(404,response.ResCode);
    EXPECT_EQ(std::nullopt,response.Response);
}


TEST_F(DummyHttpServerTest,addPostResource){
    EXPECT_TRUE(httpServer->addPostResource("TEST",[](etl::istring & resBuffer, etl::string_view req) -> HttpResponse {
        resBuffer.assign("Super Test ");
        resBuffer.append(req.cbegin(),req.cend());
        return {200,etl::string_view(resBuffer.c_str(),resBuffer.size())};
    }));
    etl::string<100> resBuf;
    etl::string<100> reqBuf;
    HttpResponse response = httpServer->post(reqBuf,resBuf,"",0,"TEST","Hello");
    EXPECT_STREQ("Super Test Hello",response.Response.value().cbegin());
    response = httpServer->post(reqBuf,resBuf,"",0,"TEST","wow");
    EXPECT_STREQ("Super Test wow",response.Response.value().cbegin());
}

TEST_F(DummyHttpServerTest,postNonExists){
    etl::string<100> resBuf;
    etl::string<100> reqBuf;
    HttpResponse response = httpServer->post(reqBuf,resBuf,"",0,"TEST","Hello");
    EXPECT_EQ(404,response.ResCode);
    EXPECT_EQ(std::nullopt,response.Response);
}

TEST_F(DummyHttpServerTest,removePostResource) {
    EXPECT_FALSE(httpServer->removePostResource("TEST"));
    EXPECT_TRUE(httpServer->addPostResource("TEST",[](etl::istring & resBuffer, etl::string_view req) -> HttpResponse {
        resBuffer.assign("Super Test ");
        resBuffer.append(req.cbegin(),req.cend());
        return {200,etl::string_view(resBuffer.c_str(),resBuffer.size())};
    }));

    etl::string<100> resBuf;
    etl::string<100> reqBuf;
    HttpResponse response = httpServer->post(reqBuf,resBuf,"",0,"TEST","Hello");
    EXPECT_STREQ("Super Test Hello",response.Response.value().cbegin());
    response = httpServer->post(reqBuf,resBuf,"",0,"TEST","wow");
    EXPECT_STREQ("Super Test wow",response.Response.value().cbegin());

    EXPECT_TRUE(httpServer->removePostResource("TEST"));
    EXPECT_FALSE(httpServer->removePostResource("TEST"));

    response = httpServer->post(reqBuf,resBuf,"",0,"TEST","Hello");
    EXPECT_EQ(404,response.ResCode);
    EXPECT_EQ(std::nullopt,response.Response);


}

TEST_F(DummyHttpServerTest,removeGetResource) {
    EXPECT_FALSE(httpServer->removeGetResource("TEST"));
    EXPECT_TRUE(httpServer->addGetResource("TEST",[](etl::istring & resBuffer) -> HttpResponse {
        resBuffer.clear();
        resBuffer.assign("Super Test");
        return {200,etl::string_view(resBuffer.c_str(),resBuffer.size())};
    }));

    etl::string<100> resBuf;
    etl::string<100> reqBuf;
    HttpResponse response = httpServer->get(resBuf,"",0,"TEST");
    EXPECT_STREQ("Super Test",response.Response.value().cbegin());
    response = httpServer->get(resBuf,"",0,"TEST");
    EXPECT_STREQ("Super Test",response.Response.value().cbegin());

    EXPECT_TRUE(httpServer->removeGetResource("TEST"));
    EXPECT_FALSE(httpServer->removeGetResource("TEST"));

    response = httpServer->get(resBuf,"",0,"TEST");
    EXPECT_EQ(404,response.ResCode);
    EXPECT_EQ(std::nullopt,response.Response);
}
