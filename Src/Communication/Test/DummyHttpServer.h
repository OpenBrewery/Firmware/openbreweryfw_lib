//
// Created by mchodzikiewicz on 15.12.18.
//

#ifndef OPENBREWERY_BSP_LINUX_DUMMYHTTPSERVER_H
#define OPENBREWERY_BSP_LINUX_DUMMYHTTPSERVER_H

#include <HttpServer.h>
#include <unordered_map>

#include "HttpClient.h"


//This one exists only to be API guard
namespace Communication {
    class DummyHttpServer : public HttpServer, public HttpClient {
    public:
        bool serve(etl::string_view host, uint16_t port) override;

        void stop() override;

        bool addGetResource(etl::string_view path, std::function<HttpResponse(etl::istring&)> function) override;

        HttpResponse Sget(etl::istring &resBuffer, etl::string_view host, int port, etl::string_view target) override;

        HttpResponse Spost(etl::istring &reqBuffer, etl::istring &resBuffer, etl::string_view host, int port, etl::string_view target,
                           etl::string_view content) override;

        bool addPostResource(etl::string_view path, std::function<HttpResponse(etl::istring&, etl::string_view)> function) override;

        void setAuthorization(etl::string_view auth_str) override;

        HttpResponse get(etl::istring & resBuffer, etl::string_view host, int port, etl::string_view target) override;
        HttpResponse post(etl::istring &reqBuffer, etl::istring & resBuffer, etl::string_view host, int port, etl::string_view target, etl::string_view content) override;


        bool removeGetResource(etl::string_view path) override;
        bool removePostResource(etl::string_view path) override;


        bool isServing() override;

        std::string Authorization;

    private:
        std::unordered_map<std::string,std::function<HttpResponse(etl::istring&)>> getResourceMap;
        std::unordered_map<std::string,std::function<HttpResponse(etl::istring&, etl::string_view)>> postResourceMap;

        bool Serving;


    };
}

#endif //OPENBREWERY_BSP_LINUX_DUMMYHTTPSERVER_H
