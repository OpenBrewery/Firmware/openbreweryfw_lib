//
// Created by mchodzikiewicz on 15.12.18.
//

#include "DummyHttpServer.h"


bool Communication::DummyHttpServer::serve(etl::string_view host, uint16_t port) {
    Serving = true;
    return false;
}

bool Communication::DummyHttpServer::addGetResource(etl::string_view path,
                                                    std::function<Communication::HttpResponse(etl::istring &)> function) {
    std::string spath(path.cbegin(),path.cend());
    getResourceMap.insert({spath, function});
    return true;
}

bool Communication::DummyHttpServer::addPostResource(etl::string_view path, std::function<Communication::HttpResponse(etl::istring &,
                                                                                                         etl::string_view)> function) {
    std::string spath(path.cbegin(),path.cend());
    postResourceMap.insert({spath, function});
    return true;
}



void Communication::DummyHttpServer::stop() {
    Serving = false;
}

Communication::HttpResponse Communication::DummyHttpServer::get(
        etl::istring & resBuffer
        , etl::string_view host
        , int port
        , etl::string_view target) {
    auto queryHandler = getResourceMap.find((std::string(target.cbegin(),target.cend())));
    if(queryHandler != getResourceMap.cend()){
        return queryHandler->second(resBuffer);
    }
    return {404,std::nullopt};
}

Communication::HttpResponse Communication::DummyHttpServer::post(
        etl::istring &reqBuffer
        , etl::istring & resBuffer
        , etl::string_view host
        , int port
        , etl::string_view target
        , etl::string_view content) {
    auto queryHandler = postResourceMap.find((std::string(target.cbegin(),target.cend())));
    if(queryHandler != postResourceMap.cend()){
        return queryHandler->second(resBuffer,content);
    }
    return {404,std::nullopt};
}

Communication::HttpResponse
Communication::DummyHttpServer::Sget(etl::istring &resBuffer, etl::string_view host, int port,
                                     etl::string_view target) {
    auto queryHandler = getResourceMap.find((std::string(target.cbegin(),target.cend())));
    if(queryHandler != getResourceMap.cend()){
        return queryHandler->second(resBuffer);
    }
    return {404,std::nullopt};
}

Communication::HttpResponse
Communication::DummyHttpServer::Spost(etl::istring &reqBuffer, etl::istring &resBuffer, etl::string_view host, int port, etl::string_view target,
                                      etl::string_view content) {
    auto queryHandler = postResourceMap.find((std::string(target.cbegin(),target.cend())));
    if(queryHandler != postResourceMap.cend()){
        return queryHandler->second(resBuffer,content);
    }
    return {404,std::nullopt};
}


bool Communication::DummyHttpServer::isServing() {
    return Serving;
}

bool Communication::DummyHttpServer::removePostResource(etl::string_view path) {
    auto queryHandler = postResourceMap.find((std::string(path.cbegin(),path.cend())));
    if(queryHandler != postResourceMap.cend()){
        postResourceMap.erase(queryHandler);
        return true;
    }
    return false;
}

bool Communication::DummyHttpServer::removeGetResource(etl::string_view path) {
    auto queryHandler = getResourceMap.find((std::string(path.cbegin(),path.cend())));
    if(queryHandler != getResourceMap.cend()){
        getResourceMap.erase(queryHandler);
        return true;
    }
    return false;
}

void Communication::DummyHttpServer::setAuthorization(etl::string_view auth_str) {
    Authorization.assign(auth_str.cbegin(),auth_str.cend());
}
