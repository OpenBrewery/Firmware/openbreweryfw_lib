//
// Created by mchodzikiewicz on 1/28/19.
//

#include "gtest/gtest.h"
#include "DummyHttpClient.h"

using namespace Communication;

class DummyHttpClientTest : public ::testing::Test {
public:
    void SetUp() {
        httpClient = new DummyHttpClient;

    };

    void TearDown() {

    };

    DummyHttpClient * httpClient;
    etl::string<256> resp;
    etl::string<256> req;

};



TEST_F(DummyHttpClientTest,Get){

    etl::string_view host("http://example.com");
    etl::string_view path("/");
    httpClient->injectedResponse = "RESPONSE";
    httpClient->ResCode = 200;

    HttpResponse res = httpClient->get(resp,host,7878,path);
    EXPECT_EQ(res.ResCode,200);
    EXPECT_STREQ(res.Response.value().data(),"RESPONSE");

    httpClient->injectedResponse = "RESPONSE2";

    EXPECT_STREQ(httpClient->get(resp,host,7878,path).Response.value().data(),"RESPONSE2");

}

TEST_F(DummyHttpClientTest,Post){
    etl::string_view host("http://example.com");
    etl::string_view path("/");
    etl::string_view content("example content");
    httpClient->injectedResponse = "RESPONSE: example_content";
    httpClient->ResCode = 200;

    HttpResponse res = httpClient->post(req,resp,host,7878,path,content);
    EXPECT_EQ(res.ResCode,200);
    EXPECT_STREQ(res.Response.value().data(),"RESPONSE: example_content");
    EXPECT_STREQ(httpClient->lastRequest.c_str(),"example content");

    httpClient->injectedResponse = "RESPONSE2";
    EXPECT_STREQ(httpClient->post(req,resp,host,7878,path,content).Response.value().data(),"RESPONSE2");

}

//TODO (mchodzikiewicz): Add tests for different responses