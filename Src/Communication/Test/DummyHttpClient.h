//
// Created by mchodzikiewicz on 15.12.18.
//

#ifndef OPENBREWERY_BSP_LINUX_DUMMYHTTPCLIENT_H
#define OPENBREWERY_BSP_LINUX_DUMMYHTTPCLIENT_H

#include "HttpClient.h"
#include "DummyHttpServer.h"

namespace Communication {
    class DummyHttpClient : public HttpClient {
    public:
        HttpResponse get(etl::istring & resBuffer, etl::string_view host, int port, etl::string_view target) override;
        HttpResponse post(etl::istring &reqBuffer, etl::istring & resBuffer, etl::string_view host, int port, etl::string_view target, etl::string_view content) override;

        HttpResponse Sget(etl::istring &resBuffer, etl::string_view host, int port, etl::string_view target) override;

        HttpResponse Spost(etl::istring &reqBuffer, etl::istring &resBuffer, etl::string_view host, int port, etl::string_view target,
                           etl::string_view content) override;

        void setAuthorization(etl::string_view auth_str) override;

        std::string lastRequest;
        std::string injectedResponse;
        uint16_t ResCode;

        std::string Authorization;
    };
}

#endif //OPENBREWERY_BSP_LINUX_DUMMYHTTPCLIENT_H
