# Sensors inteface

Depending on your target platform, following approaches are recommended:

### Asynchronous:
If you are writing application that will not have threads (or you don't want to overuse them on small platform), yet does plenty things at the time, you can us async interface:

Use method requestMeasurement(Observer &) on sensor and handle the result inside your observer.

### Synchronous:
If you are willing to keep simplier program flow, you can use blocking read() method, and either operate from your main thread or call it in separate one.



### Implement your own sensor:
Sensor API is designed not to force any kind of underlying interface. 

1. Create (or use existing) sensor type by inheriting Sensor template with specific measurement result type.
2. In your final class, inherit your sensor type.
3. Override blocking read(), for example: request measurement, wait for measurement to be done, return the result
4. Do one of following: 
    * Override async 
        ```cpp 
        bool requestMeasurement(Observer &)
        ```
         method, ensuring that it will return as fast as possible (make sure to return false if measurement is illegal in current state)
         
         place 
         ```cpp
         void notify_observers(measurement)
         ```
      in your callback
      
    * Place 
        ```cpp
        static_assert(false,"Async operation impossible");
        ```
        to ensure compile-time error on illegal use
        