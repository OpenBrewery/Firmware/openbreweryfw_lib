//
// Created by mchodzikiewicz on 2/12/19.
//

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include <deque>
#include <etl/deque.h>
#include <Thermometer.h>
#include <Accelerometer.h>
#include "SensorSimulator.h"

using testing::ElementsAre;
using namespace Sensor;

class SensorSimulatorTest : public ::testing::Test {
public:
    void SetUp() override {

    };
    void TearDown() override {

    };


};

template<typename SensorT>
class DummySensorObserver : public SensorT::Observer {
public:

    void notification(std::optional<typename SensorT::measurement_type> notification) { //This does not override, it's CRTP
        notification_cnt++;
        last_notification = notification;
    }

    unsigned int notification_cnt = 0;
    std::optional<typename SensorT::measurement_type> last_notification;

};

TEST_F(SensorSimulatorTest,ThermometerBlocking){
    SensorSimulator<Thermometer> sensor;

    EXPECT_EQ(std::nullopt,sensor.read());
    sensor.addMeasuement(123.123);
    EXPECT_FLOAT_EQ(123.123,sensor.read().value());


    EXPECT_EQ(std::nullopt,sensor.read());

    sensor.addMeasuement(1);
    EXPECT_FLOAT_EQ(1,sensor.read().value());

    //behavior on stacked measurements
    sensor.addMeasuement(2);
    sensor.addMeasuement(3);
    sensor.addMeasuement(4);
    sensor.addMeasuement(5);

    EXPECT_FLOAT_EQ(2,sensor.read().value());
    EXPECT_FLOAT_EQ(3,sensor.read().value());
    EXPECT_FLOAT_EQ(4,sensor.read().value());
    EXPECT_FLOAT_EQ(5,sensor.read().value());
    EXPECT_EQ(std::nullopt,sensor.read());
}

TEST_F(SensorSimulatorTest,AccelerometerBlocking){
    SensorSimulator<Accelerometer> sensor;

    EXPECT_EQ(std::nullopt,sensor.read());
    sensor.addMeasuement({1,2,3});
    EXPECT_EQ(Geometry::Point3D<int>(1,2,3),sensor.read().value());


    EXPECT_EQ(std::nullopt,sensor.read());

    sensor.addMeasuement({1,1,1});
    EXPECT_EQ(Geometry::Point3D<int>(1,1,1),sensor.read().value());

    //behavior on stacked measurements
    sensor.addMeasuement({2,2,2});
    sensor.addMeasuement({3,3,3});
    sensor.addMeasuement({4,4,4});
    sensor.addMeasuement({5,5,5});

    EXPECT_EQ(Geometry::Point3D<int>(2,2,2),sensor.read().value());
    EXPECT_EQ(Geometry::Point3D<int>(3,3,3),sensor.read().value());
    EXPECT_EQ(Geometry::Point3D<int>(4,4,4),sensor.read().value());
    EXPECT_EQ(Geometry::Point3D<int>(5,5,5),sensor.read().value());
    EXPECT_EQ(std::nullopt,sensor.read());
}



TEST_F(SensorSimulatorTest,ThermometerAsync){
    using namespace std::literals;
    SensorSimulator<Thermometer> sensor;
    DummySensorObserver<Thermometer> observer;
    EXPECT_TRUE(sensor.requestMeasurement(observer));  //behavior on empty queue
    EXPECT_FALSE(sensor.requestMeasurement(observer));  //behavior on request when another is processed

    EXPECT_EQ(observer.notification_cnt,0);             //no notification until fed
    EXPECT_EQ(std::nullopt,observer.last_notification);
    sensor.addMeasuement(123.123);
    std::this_thread::sleep_for(10ms);
    EXPECT_EQ(observer.notification_cnt,1);             //subscriber is notified
    EXPECT_FLOAT_EQ(123.123,observer.last_notification.value());

    EXPECT_TRUE(sensor.requestMeasurement(observer));  //behavior on emptied
    EXPECT_EQ(observer.notification_cnt,1);             //no notification until fed
    EXPECT_FLOAT_EQ(123.123,observer.last_notification.value());

    sensor.addMeasuement(1);
    std::this_thread::sleep_for(10ms);
    EXPECT_EQ(observer.notification_cnt,2);             //subscriber notified again
    EXPECT_FLOAT_EQ(1,observer.last_notification.value());

    //behavior on stacked measurements
    sensor.addMeasuement(2);
    sensor.addMeasuement(3);
    sensor.addMeasuement(4);
    sensor.addMeasuement(5);

    EXPECT_TRUE(sensor.requestMeasurement(observer));
    std::this_thread::sleep_for(10ms);
    EXPECT_EQ(observer.notification_cnt,3);
    EXPECT_FLOAT_EQ(2,observer.last_notification.value());

    EXPECT_TRUE(sensor.requestMeasurement(observer));
    std::this_thread::sleep_for(10ms);
    EXPECT_EQ(observer.notification_cnt,4);
    EXPECT_FLOAT_EQ(3,observer.last_notification.value());

    EXPECT_TRUE(sensor.requestMeasurement(observer));
    std::this_thread::sleep_for(10ms);
    EXPECT_EQ(observer.notification_cnt,5);
    EXPECT_FLOAT_EQ(4,observer.last_notification.value());

    EXPECT_TRUE(sensor.requestMeasurement(observer));
    std::this_thread::sleep_for(10ms);
    EXPECT_EQ(observer.notification_cnt,6);
    EXPECT_FLOAT_EQ(5,observer.last_notification.value());

    EXPECT_TRUE(sensor.requestMeasurement(observer));
    std::this_thread::sleep_for(10ms);
    EXPECT_EQ(observer.notification_cnt,6);
    EXPECT_FLOAT_EQ(5,observer.last_notification.value());

    EXPECT_FALSE(sensor.requestMeasurement(observer));
    sensor.addMeasuement(6);
    std::this_thread::sleep_for(10ms);
    EXPECT_EQ(observer.notification_cnt,7);
    EXPECT_FLOAT_EQ(6,observer.last_notification.value());
}

TEST_F(SensorSimulatorTest,AccelerometerAsync){
    using namespace std::literals;
    SensorSimulator<Accelerometer> sensor;
    DummySensorObserver<Accelerometer> observer;
    EXPECT_TRUE(sensor.requestMeasurement(observer));  //behavior on empty queue
    EXPECT_FALSE(sensor.requestMeasurement(observer));  //behavior on request when another is processed

    EXPECT_EQ(observer.notification_cnt,0);             //no notification until fed
    EXPECT_EQ(std::nullopt,observer.last_notification);
    sensor.addMeasuement({1,2,3});
    std::this_thread::sleep_for(10ms);
    EXPECT_EQ(observer.notification_cnt,1);             //no notification until fed
    EXPECT_EQ(Geometry::Point3D<int>(1,2,3),observer.last_notification.value());

    EXPECT_TRUE(sensor.requestMeasurement(observer));  //behavior on emptied
    EXPECT_EQ(observer.notification_cnt,1);             //no notification until fed
    EXPECT_EQ(Geometry::Point3D<int>(1,2,3),observer.last_notification.value());

    sensor.addMeasuement({1,1,1});
    std::this_thread::sleep_for(10ms);
    EXPECT_EQ(observer.notification_cnt,2);             //no notification until fed
    EXPECT_EQ(Geometry::Point3D<int>(1,1,1),observer.last_notification.value());

    //behavior on stacked measurements
    sensor.addMeasuement({2,2,2});
    sensor.addMeasuement({3,3,3});
    sensor.addMeasuement({4,4,4});
    sensor.addMeasuement({5,5,5});

    EXPECT_TRUE(sensor.requestMeasurement(observer));
    std::this_thread::sleep_for(10ms);
    EXPECT_EQ(observer.notification_cnt,3);
    EXPECT_EQ(Geometry::Point3D<int>(2,2,2),observer.last_notification.value());

    EXPECT_TRUE(sensor.requestMeasurement(observer));
    std::this_thread::sleep_for(10ms);
    EXPECT_EQ(observer.notification_cnt,4);
    EXPECT_EQ(Geometry::Point3D<int>(3,3,3),observer.last_notification.value());

    EXPECT_TRUE(sensor.requestMeasurement(observer));
    std::this_thread::sleep_for(10ms);
    EXPECT_EQ(observer.notification_cnt,5);
    EXPECT_EQ(Geometry::Point3D<int>(4,4,4),observer.last_notification.value());

    EXPECT_TRUE(sensor.requestMeasurement(observer));
    std::this_thread::sleep_for(10ms);
    EXPECT_EQ(observer.notification_cnt,6);
    EXPECT_EQ(Geometry::Point3D<int>(5,5,5),observer.last_notification.value());

    EXPECT_TRUE(sensor.requestMeasurement(observer));
    std::this_thread::sleep_for(10ms);
    EXPECT_EQ(observer.notification_cnt,6);
    EXPECT_EQ(Geometry::Point3D<int>(5,5,5),observer.last_notification.value());

    EXPECT_FALSE(sensor.requestMeasurement(observer));
    sensor.addMeasuement({6,6,6});
    std::this_thread::sleep_for(10ms);
    EXPECT_EQ(observer.notification_cnt,7);
    EXPECT_EQ(Geometry::Point3D<int>(6,6,6),observer.last_notification.value());
}
