//
// Created by michal on 02.12.18.
//

#include "DummyI2CInterface.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "SHT21.h"
#include "etl/vector.h"



const uint8_t TRIGGER_TEMP_MEASURE_NOHOLD=0xF3;
const uint8_t TRIGGER_HUMD_MEASURE_NOHOLD=0xF5;


using namespace Peripheral;
using testing::ElementsAre;

class SHT21Test : public ::testing::Test {
public:
    void SetUp() override {
        interface = new DummyI2CInterface();
    };
    void TearDown() override {
        wbuffer.clear();
        rbuffer.clear();
        interface->Request.clear();
        interface->Response.clear();
        delete interface;
    };

    Peripheral::DummyI2CInterface * interface;
    etl::vector<uint8_t,1024> wbuffer;
    etl::vector<uint8_t,1024> rbuffer;
};


TEST_F(SHT21Test,Init){
    Sensor::SHT21 sensor(*interface,0x40,wbuffer,rbuffer);
    EXPECT_EQ(sensor.address(),0x40);
    EXPECT_TRUE(sensor.init());
}


//TODO: FOLLOWING TESTS ARE WRITTEN TO PASS, CALCULATE VALS FROM DATASHEET AND CHECK
TEST_F(SHT21Test,getTemperature10){
    Sensor::SHT21 sensor(*interface,0x40,wbuffer,rbuffer);
    interface->Response = {82,211};
    EXPECT_NEAR(sensor.thermometer.read().value(),10,0.005);
    EXPECT_THAT(interface->Request,ElementsAre(0x40));

}


TEST_F(SHT21Test,getHumidity10){
    Sensor::SHT21 sensor(*interface,0x40,wbuffer,rbuffer);
    interface->Response = {32,197};
    EXPECT_NEAR(sensor.humiditySensor.read().value(),10,0.001);
    EXPECT_THAT(interface->Request,ElementsAre(0x40));
}


//TODO: add tests that ensures that you cannot read measurement without requesting it

//TODO: add tests for asynchronous read
