//
// Created by michal on 14.10.18.
//
#include "DummyI2CInterface.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "MMA8452.h"
#include "etl/vector.h"

using namespace Peripheral;
using testing::ElementsAre;

class MMA8452Test : public ::testing::Test {
public:
    void SetUp() override {
        interface = new DummyI2CInterface();
        wbuffer = new etl::vector<uint8_t,1024>;
        rbuffer = new etl::vector<uint8_t,1024>;
    };
    void TearDown() override {
        delete wbuffer;
        delete rbuffer;
        interface->Request.clear();
        interface->Response.clear();
        delete interface;
    };

    Peripheral::DummyI2CInterface * interface;
    etl::vector<uint8_t,1024> * wbuffer;
    etl::vector<uint8_t,1024> * rbuffer;
};


TEST_F(MMA8452Test,Init){
    Sensor::MMA8452 sensor(*interface,Peripheral::I2CAddress(100),*wbuffer,*rbuffer);
    EXPECT_EQ(sensor.address(),100);
    EXPECT_TRUE(sensor.init());
    EXPECT_THAT(interface->Request,ElementsAre(100,14,0,100,42,45));
}

TEST_F(MMA8452Test,getMeasurement){
    Sensor::MMA8452 sensor(*interface,Peripheral::I2CAddress(100),*wbuffer,*rbuffer);

    interface->Response = {0,0,0,0,0,0};
    EXPECT_EQ(sensor.read().value(),Geometry::Point3D<int>(0,0,0));
    EXPECT_THAT(interface->Request,ElementsAre(100,1));


    interface->Response = {0x10,0x00,0x11,0x01,0x12,0x02};
    EXPECT_EQ(sensor.read().value(),Geometry::Point3D<int>(4096,4353,4610));
    EXPECT_THAT(interface->Request,ElementsAre(100,1));
}

TEST_F(MMA8452Test,getNegativeMeasurement){
    Sensor::MMA8452 sensor(*interface,Peripheral::I2CAddress(100),*wbuffer,*rbuffer);

    interface->Response = {0xFF,0xFF,0xFF,0xFE,0xFF,0xFD};
    EXPECT_EQ(sensor.read().value(),Geometry::Point3D<int>(-1,-2,-3));
    EXPECT_THAT(interface->Request,ElementsAre(100,1));
}
//TODO: add tests for asynchronous reads