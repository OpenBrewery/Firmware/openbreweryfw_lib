//
// Created by michal on 02.12.18.
//

#include <SHT21.h>
#include <thread>


#define TRIGGER_TEMP_MEASURE_NOHOLD  0xF3
#define TRIGGER_HUMD_MEASURE_NOHOLD  0xF5


Sensor::SHT21::SHT21(
        Peripheral::I2CInterface & iface
        , Peripheral::I2CAddress addr
        , etl::ivector<uint8_t> &wbuffer
        , etl::ivector<uint8_t> &rbuffer)
        : thermometer(*this,addr,wbuffer, rbuffer), humiditySensor(*this,addr,wbuffer, rbuffer), Interface(iface), lock(false) {

}

uint8_t Sensor::SHT21::address() {
    return humiditySensor.address();
}

bool Sensor::SHT21::init() {
    return true;
}


Sensor::SHT21::SHT21Thermometer::SHT21Thermometer(SHT21 &parent, uint8_t addr, etl::ivector<uint8_t> &wbuffer,
                                                  etl::ivector<uint8_t> &rbuffer) : Parent(parent), I2CDevice(parent.Interface,addr,wbuffer, rbuffer) {

}

std::optional<Sensor::Temperature> Sensor::SHT21::SHT21Thermometer::read() {
    using namespace std::literals;
    if(Parent.lock) return std::nullopt;
    write_buf.clear();
    write_buf.push_back(TRIGGER_TEMP_MEASURE_NOHOLD);
    write();
    std::this_thread::sleep_for(100ms);     //TODO: substitute this with etl timer
    write_buf.clear();
    auto req = request(2);
    return (-46.85 + 175.72 / 65536.0 * (float)(req.at(0)<< 8 | (req.at(1) & 0xFF)));
}

bool Sensor::SHT21::SHT21Thermometer::requestMeasurement(Observer & caller) {
//    write_buf.clear();
//    write_buf.push_back(TRIGGER_TEMP_MEASURE_NOHOLD);
//    return write();
    return false;
}

Sensor::SHT21::SHT21HumiditySensor::SHT21HumiditySensor(SHT21 &parent, uint8_t addr, etl::ivector<uint8_t> &wbuffer,
                                                        etl::ivector<uint8_t> &rbuffer) : Parent(parent), I2CDevice(parent.Interface,addr,wbuffer, rbuffer)  {

}

std::optional<Sensor::Humidity> Sensor::SHT21::SHT21HumiditySensor::read() {
    using namespace std::literals;
    if(Parent.lock) return std::nullopt;
    write_buf.clear();
    write_buf.push_back(TRIGGER_HUMD_MEASURE_NOHOLD);
    write();
    std::this_thread::sleep_for(100ms); //TODO: substitute this with etl timer
    write_buf.clear();
    auto req = request(2);
    return (-6.0 + 125.0 / 65536.0 * (float)(req.at(0)<< 8 | (req.at(1) & 0xFF)));
}

bool Sensor::SHT21::SHT21HumiditySensor::requestMeasurement(Observer & caller) {
//    write_buf.clear();
//    write_buf.push_back(TRIGGER_HUMD_MEASURE_NOHOLD);
//    return write();
    return false;
}
