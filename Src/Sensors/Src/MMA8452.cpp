//
// Created by michal on 14.10.18.
//
#include "MMA8452.h"


constexpr uint8_t MMA8452Q_STATUS_ADDR = 0x00;
constexpr uint8_t MMA8452Q_OUT_X_MSB_ADDR = 0x01;
constexpr uint8_t MMA8452Q_OUT_X_LSB_ADDR = 0x02;
constexpr uint8_t MMA8452Q_OUT_Y_MSB_ADDR = 0x03;
constexpr uint8_t MMA8452Q_OUT_Y_LSB_ADDR = 0x04;
constexpr uint8_t MMA8452Q_OUT_Z_MSB_ADDR = 0x05;
constexpr uint8_t MMA8452Q_OUT_Z_LSB_ADDR = 0x06;

constexpr uint8_t MMA8452Q_SYSMOD_ADDR = 0x0B;
constexpr uint8_t MMA8452Q_INT_SOURCE_ADDR = 0x0C;
constexpr uint8_t MMA8452Q_WHO_AM_I_ADDR = 0x0D;
constexpr uint8_t MMA8452Q_XYZ_DATA_CFG_ADDR = 0x0E;
constexpr uint8_t MMA8452Q_HP_FILTER_CUTOFF_ADDR = 0x0F;
constexpr uint8_t MMA8452Q_PL_STATUS_ADDR = 0x10;
constexpr uint8_t MMA8452Q_PL_CFG_ADDR = 0x11;
constexpr uint8_t MMA8452Q_PL_COUNT_ADDR = 0x12;
constexpr uint8_t MMA8452Q_PL_BF_ZCOMP_ADDR = 0x13;
constexpr uint8_t MMA8452Q_PL_THS_REG_ADDR = 0x14;
constexpr uint8_t MMA8452Q_PL_MT_CFG_ADDR = 0x15;
constexpr uint8_t MMA8452Q_PL_MT_SRC_ADDR = 0x16;
constexpr uint8_t MMA8452Q_PL_MT_THS_ADDR = 0x17;
constexpr uint8_t MMA8452Q_PL_MT_COUNT_ADDR = 0x18;

constexpr uint8_t MMA8452Q_TRANSIENT_CFG_ADDR = 0x1D;
constexpr uint8_t MMA8452Q_TRANSIENT_SRC_ADDR = 0x1E;
constexpr uint8_t MMA8452Q_TRANSIENT_THS_ADDR = 0x1F;
constexpr uint8_t MMA8452Q_TRANSIENT_COUNT_ADDR = 0x20;

constexpr uint8_t MMA8452Q_PULSE_CFG_ADDR = 0x21;
constexpr uint8_t MMA8452Q_PULSE_SRC_ADDR = 0x22;
constexpr uint8_t MMA8452Q_PULSE_THSX_ADDR = 0x23;
constexpr uint8_t MMA8452Q_PULSE_THSY_ADDR = 0x24;
constexpr uint8_t MMA8452Q_PULSE_THSZ_ADDR = 0x25;
constexpr uint8_t MMA8452Q_PULSE_TMLT_ADDR = 0x26;
constexpr uint8_t MMA8452Q_PULSE_LTCY_ADDR = 0x27;
constexpr uint8_t MMA8452Q_PULSE_WIND_ADDR = 0x28;

constexpr uint8_t MMA8452Q_ASLP_COUNT_ADDR = 0x29;

constexpr uint8_t MMA8452Q_CTRL_REG1_ADDR = 0x2A;
constexpr uint8_t MMA8452Q_CTRL_REG2_ADDR = 0x2B;
constexpr uint8_t MMA8452Q_CTRL_REG3_ADDR = 0x2C;
constexpr uint8_t MMA8452Q_CTRL_REG4_ADDR = 0x2D;
constexpr uint8_t MMA8452Q_CTRL_REG5_ADDR = 0x2E;

constexpr uint8_t MMA8452Q_OFF_X_ADDR = 0x31;
constexpr uint8_t MMA8452Q_OFF_Y_ADDR = 0x30;
constexpr uint8_t MMA8452Q_OFF_Z_ADDR = 0x31;

constexpr uint8_t MMA8452Q_12bit_mode = 0;

constexpr uint8_t MMA8452Q_CTRL_REG1_wakeup = 1;
constexpr uint8_t MMA8452Q_CTRL_REG1_LNOISE = 1<<2;
constexpr uint8_t MMA8452Q_CTRL_REG1_ODR_800Hz = 0<<3;
constexpr uint8_t MMA8452Q_CTRL_REG1_ODR_400Hz = 1<<3;
constexpr uint8_t MMA8452Q_CTRL_REG1_ODR_200Hz = 2<<3;
constexpr uint8_t MMA8452Q_CTRL_REG1_ODR_100Hz = 3<<3;
constexpr uint8_t MMA8452Q_CTRL_REG1_ODR_50Hz = 4<<3;
constexpr uint8_t MMA8452Q_CTRL_REG1_ODR_12_5Hz = 5<<3;
constexpr uint8_t MMA8452Q_CTRL_REG1_ODR_6_25Hz = 6<<3;
constexpr uint8_t MMA8452Q_CTRL_REG1_ODR_1_56Hz = 7<<3;



Sensor::MMA8452::MMA8452(
        Peripheral::I2CInterface & iface
        , Peripheral::I2CAddress addr
        , etl::ivector<uint8_t> & wbuffer
        , etl::ivector<uint8_t> & rbuffer)
        : I2CDevice(iface,addr,wbuffer,rbuffer), Accelerometer() {

}

bool Sensor::MMA8452::init() {
    write_buf.clear();  //TODO (mchodzikiewicz): zeroing all elements is nor needed nor effective
    write_buf.push_back(MMA8452Q_XYZ_DATA_CFG_ADDR);
    write_buf.push_back(MMA8452Q_12bit_mode);
    if(!write()) return false;

    write_buf.clear(); //TODO (mchodzikiewicz): zeroing all elements is nor needed nor effective
    write_buf.push_back(MMA8452Q_CTRL_REG1_ADDR);
    write_buf.push_back(MMA8452Q_CTRL_REG1_wakeup | MMA8452Q_CTRL_REG1_LNOISE | MMA8452Q_CTRL_REG1_ODR_12_5Hz);
    return write();
}

std::optional<Geometry::Point3D<int>> Sensor::MMA8452::read() {
    write_buf.clear();
    write_buf.push_back(MMA8452Q_OUT_X_MSB_ADDR);

    etl::array_view<uint8_t> response = request(6);
    if(response.size() != 6) return std::nullopt;
    return Geometry::Point3D<int>(
            static_cast<int16_t>(response[0]<<8 | response[1])
            , static_cast<int16_t>(response[2]<<8 | response[3])
            , static_cast<int16_t>(response[4]<<8 | response[5])
    );
}

bool Sensor::MMA8452::requestMeasurement(Observer & caller) {
    return false;
}

Sensor::MMA8452::SysMod Sensor::MMA8452::getSysMod() {
    write_buf.clear();
    write_buf.push_back(MMA8452Q_SYSMOD_ADDR);
    return static_cast<SysMod>(request(1).at(0));
}


