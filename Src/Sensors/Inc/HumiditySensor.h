//
// Created by mchodzikiewicz on 10/6/18.
//

#ifndef OPENBREWERYFW_LIB_HUMIDITYSENSOR_H
#define OPENBREWERYFW_LIB_HUMIDITYSENSOR_H

#include "Sensor.h"

namespace Sensor{
    using Humidity = float;
    class HumiditySensor : public Sensor<Humidity> {
    public:
        ~HumiditySensor() override = default;
    };
}


#endif //OPENBREWERYFW_LIB_HUMIDITYSENSOR_H
