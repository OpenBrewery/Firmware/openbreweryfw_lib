//
// Created by michal on 02.12.18.
//

#ifndef OPENBREWERYFW_LIB_SHT21_H
#define OPENBREWERYFW_LIB_SHT21_H

#include "I2CInterface.h"
#include "Thermometer.h"
#include "HumiditySensor.h"


namespace Sensor {
//    etl::message

    class SHT21 {
    public:
        friend class SHT21Thermometer;
        friend class SHT21HumiditySensor;
        class SHT21Thermometer : public Thermometer, public Peripheral::I2CDevice {
        public:
            explicit SHT21Thermometer(SHT21 & parent, uint8_t addr, etl::ivector<uint8_t> & wbuffer, etl::ivector<uint8_t> & rbuffer);

            std::optional<Temperature> read() override;
            bool requestMeasurement(Observer & caller) override;
        private:
            SHT21 & Parent;


        };

        class SHT21HumiditySensor : public HumiditySensor, public Peripheral::I2CDevice {
        public:
            explicit SHT21HumiditySensor(SHT21 & parent, uint8_t addr, etl::ivector<uint8_t> & wbuffer, etl::ivector<uint8_t> & rbuffer);

            std::optional<Humidity> read() override;
            bool requestMeasurement(Observer & caller) override;
        private:
            SHT21 & Parent;

        };

        SHT21(Peripheral::I2CInterface & iface, Peripheral::I2CAddress addr, etl::ivector<uint8_t> & wbuffer, etl::ivector<uint8_t> & rbuffer);
        bool init();
        uint8_t address();

    private:
        Peripheral::I2CInterface & Interface;
        bool lock;

    public:
        SHT21Thermometer thermometer;
        SHT21HumiditySensor humiditySensor;
    };
}

#endif //OPENBREWERYFW_LIB_SHT21_H
