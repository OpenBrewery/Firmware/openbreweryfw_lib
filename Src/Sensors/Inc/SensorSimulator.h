//
// Created by mchodzikiewicz on 2/12/19.
//

#ifndef BEERBOB_APPLICATION_SENSORSIMULATOR_H
#define BEERBOB_APPLICATION_SENSORSIMULATOR_H

#include <optional>
#include <thread>
#include <etl/observer.h>
#include <deque>

namespace Sensor {
    template<class SensorT>
    class SensorSimulator : public SensorT {
    public:
        using Meas = typename SensorT::measurement_type;
        using Observer = typename SensorT::Observer;
        using Observable = typename etl::observable<Observer,1>;

        SensorSimulator() : thread(nullptr) {}

        ~SensorSimulator(){
            if(thread != nullptr) {
                thread->join();
                delete thread;
            }
        }

        std::optional<Meas> read() override {
            if(Deque.empty()) return std::nullopt;
            Meas m = Deque.front();
            Deque.pop_front();
            return m;
        }

        bool requestMeasurement(Observer & caller) override {
            if(Observable::number_of_observers()) return false; //TODO (mchodzikiewicz): this will cause errors when we'll allow multiple observers
            using namespace std::chrono_literals;
            Observable::add_observer(caller);
            thread = new std::thread([this](){waitForFeed();});
            return true;
        }

        void addMeasuement(Meas measurement){
            Deque.push_back(measurement);
        }

        void insertNextMeasurement(Meas measurement){
            Deque.push_front(measurement);
        }


    private:
        std::deque<Meas> Deque;
        std::thread * thread;
        void waitForFeed(){
            while(Deque.empty());
            std::optional<Meas> meas = read();
            Observable::notify_observers(meas);
            Observable::clear_observers();  //TODO (mchodzikiewicz): this will cause errors when we'll allow multiple observers
        }
    };
}


#endif //BEERBOB_APPLICATION_SENSORSIMULATOR_H
