//
// Created by mchodzikiewicz on 10/6/18.
//

#ifndef OPENBREWERYFW_LIB_ACCELEROMETER_H
#define OPENBREWERYFW_LIB_ACCELEROMETER_H

#include "Sensor.h"
#include "Point3D.h"

namespace Sensor {
    class Accelerometer : public Sensor<Geometry::Point3D<int>> {
    public:

        ~Accelerometer() override = default;

    };

}

#endif //OPENBREWERYFW_LIB_ACCELEROMETER_H
