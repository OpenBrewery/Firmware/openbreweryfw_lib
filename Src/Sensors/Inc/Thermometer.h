//
// Created by mchodzikiewicz on 10/6/18.
//

#ifndef OPENBREWERYFW_LIB_THERMOMETER_H
#define OPENBREWERYFW_LIB_THERMOMETER_H

#include "Sensor.h"

namespace Sensor {
    using Temperature = float; //TODO (mchodzikiewicz) It would be nice to provide some type safety here
    class Thermometer : public Sensor<Temperature>{
//        ~Thermometer() override = default;
    };
}

#endif //OPENBREWERYFW_LIB_THERMOMETER_H
