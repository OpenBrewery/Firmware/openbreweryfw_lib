//
// Created by michal on 14.10.18.
//

#ifndef OPENBREWERYFW_LIB_MMA8452_H
#define OPENBREWERYFW_LIB_MMA8452_H

#include <I2CInterface.h>
#include "Accelerometer.h"

namespace Sensor {
    class MMA8452 : public Accelerometer, public Peripheral::I2CDevice {
    public:
        MMA8452(Peripheral::I2CInterface & iface, Peripheral::I2CAddress addr, etl::ivector<uint8_t> & wbuffer, etl::ivector<uint8_t> & rbuffer);

        bool init();
        std::optional<Geometry::Point3D<int>> read() override;


        bool requestMeasurement(Observer & caller) override;

        enum SysMod {Standby = 0,Wake = 1, Sleep = 2};

        SysMod getSysMod();

    };
}

#endif //OPENBREWERYFW_LIB_MMA8452_H
