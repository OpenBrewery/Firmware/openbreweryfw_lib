//
// Created by mchodzikiewicz on 10/6/18.
//

#ifndef OPENBREWERYFW_LIB_SENSOR_H
#define OPENBREWERYFW_LIB_SENSOR_H

#include <memory>
#include "PhysicalResource.h"
#include "AsyncInterface.h"
#include "etl/observer.h"
#include <optional>

namespace Sensor {
    template<typename Measurement>
class Sensor : public Common::PhysicalResource, protected etl::observable<etl::observer<std::optional<Measurement>>,1> {
    public:
        using Observer = etl::observer<std::optional<Measurement>>;

        typedef Measurement measurement_type;


        virtual std::optional<Measurement> read() = 0;
        virtual bool requestMeasurement(Observer & caller) = 0;
    };
}

#endif //OPENBREWERYFW_LIB_SENSOR_H
