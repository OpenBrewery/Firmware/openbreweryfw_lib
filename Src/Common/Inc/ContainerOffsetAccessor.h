//
// Created by mchodzikiewicz on 09.12.18.
//

#ifndef OPENBREWERYFW_LIB_CONTAINEROFFSETACCESSOR_H
#define OPENBREWERYFW_LIB_CONTAINEROFFSETACCESSOR_H

#include <cstddef>
#include <cstdint>
#include <initializer_list>
#include <algorithm>


template<typename Container, size_t front_offset, size_t back_offset = 0>
class ContainerOffsetAccessor {
public:
    ContainerOffsetAccessor(Container &  cnt) : container(cnt) {}

    void clear() {
        container.erase(container.begin()+front_offset,container.end()-back_offset);
    }

    typename Container::value_type at(size_t n){
        return container.at(n+front_offset);
    }

    typename Container::value_type * data(){
        return container.data();
    }

    size_t size() {
        return container.size()-(front_offset+back_offset);
    }

    typename Container::iterator begin(){
        return container.begin()+front_offset;
    }

    typename Container::const_iterator cbegin(){
        return container.cbegin()+front_offset;
    }

    typename Container::iterator end(){
        return container.end()-back_offset;
    }

    typename Container::const_iterator cend(){
        return container.cend()-back_offset;
    }

    void push_back(const uint8_t& val){
        for(int i=container.size();i<front_offset;i++){
            container.push_back(0);
        }
        container.push_back(val);
    }

    ContainerOffsetAccessor & operator=(std::initializer_list<typename Container::value_type> cont){
        std::copy(cont.begin(),cont.end(),container.begin()+front_offset);
        return *this;
    }

private:
    Container & container;
};


#endif //OPENBREWERYFW_LIB_CONTAINEROFFSETACCESSOR_H
