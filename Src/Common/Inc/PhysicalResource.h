//
// Created by mchodzikiewicz on 10/6/18.
//

#ifndef OPENBREWERYFW_LIB_PHYSICALRESOURCE_H
#define OPENBREWERYFW_LIB_PHYSICALRESOURCE_H

namespace Common {
    class PhysicalResource {
    public:
        PhysicalResource() {}

        virtual ~PhysicalResource() = default;

        PhysicalResource(const PhysicalResource &) = delete;

        PhysicalResource &operator=(const PhysicalResource &) = delete;
    };

}
#endif //OPENBREWERYFW_LIB_PHYSICALRESOURCE_H
