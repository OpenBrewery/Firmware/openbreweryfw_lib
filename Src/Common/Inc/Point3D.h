//
// Created by mchodzikiewicz on 10/13/18.
//

#ifndef OPENBREWERYFW_LIB_GEOMETRY_H
#define OPENBREWERYFW_LIB_GEOMETRY_H

#include <numeric>
#include <cmath>
#include <etl/array_view.h>

namespace Geometry {
    constexpr float pi = std::acos(-1);

    template<typename T>
    class Point3D {
    public:
        Point3D() : Point3D(0,0,0) {}
        Point3D(T x,T y,T z) : X(x),Y(y),Z(z) {}

        template<typename ArgT>
        Point3D(const Point3D<ArgT> & point){
            X = point.X;
            Y = point.Y;
            Z = point.Z;
        }


        void clear() {X=0;Y=0;Z=0;}

        enum class Origin {
            X_negative, //In normal position, gravity gives positive readout on X axis - negate sign and swap X with Z axis
            X_positive, //In normal position, gravity gives positive readout on X axis - swap X with Z axis
            Y_positive, //In normal position, gravity gives positive readout on Y axis - negate sign and swap Y with Z axis
            Y_negative, //In normal position, gravity gives positive readout on Y axis - swap Y with Z axis
            Z_positive, //In normal position, gravity gives positive readout on Z axis - negate Z sign during reorientation
            Z_negative  //In normal position, gravity gives negative readout on Z axis - no action during reorientation
        };

        static Point3D<T> reorientate(const Point3D<T> & p, Origin origin) {
            switch(origin) {
                case Origin::Y_negative:
                    return {p.Z,p.X,p.Y};
                case Origin::Z_negative:
                    return p;
                default:
                    return {0,0,0};
            }
        }

        template<typename ArgT>
        static Point3D avg(etl::array_view<Point3D<ArgT>> vec){
            return std::accumulate(vec.begin(),vec.end(),Point3D<ArgT>(0,0,0))/ static_cast<ArgT>(vec.size());
        }


        template<typename ArgT>
        Point3D<T> operator+(const Point3D<ArgT> & point){
            return Point3D(X + point.X, Y + point.Y, Z + point.Z);
        }

        template<typename ArgT>
        Point3D<T> operator+(const ArgT & value){
            return Point3D(X + value, Y + value, Z + value);
        }

        template<typename ArgT>
        Point3D<T> operator-(const Point3D<ArgT> & point){
            return Point3D(X - point.X, Y - point.Y, Z - point.Z);
        }

        template<typename ArgT>
        Point3D<T> operator-(const ArgT & value){
            return Point3D(X - value, Y - value, Z - value);
        }

        template<typename ArgT>
        Point3D<T> operator*(const Point3D<ArgT> & point){
            return Point3D(X * point.X, Y * point.Y, Z * point.Z);
        }

        template<typename ArgT>
        Point3D<T> operator*(const ArgT & value){
            return Point3D(X * value, Y * value, Z * value);
        }

        template<typename ArgT>
        Point3D<T> operator/(const Point3D<ArgT> & point){
            return Point3D(X / point.X, Y / point.Y, Z / point.Z);
        }

        template<typename ArgT>
        Point3D<T> operator/(const ArgT & value){
            return Point3D(X / value, Y / value, Z / value);
        }

        template<typename ArgT>
        Point3D<T> operator+=(const Point3D<ArgT> & point){
            X += point.X;
            Y += point.Y;
            Z += point.Z;
            return *this;
        }

        template<typename ArgT>
        Point3D<T> & operator-=(const Point3D<ArgT> & point){
            X -= point.X;
            Y -= point.Y;
            Z -= point.Z;
            return *this;
        }

        template<typename ArgT>
        Point3D<T> & operator*=(const Point3D<ArgT> & point){
            X *= point.X;
            Y *= point.Y;
            Z *= point.Z;
            return *this;
        }

        template<typename ArgT>
        Point3D<T> & operator/=(const Point3D<ArgT> & point){
            X /= point.X;
            Y /= point.Y;
            Z /= point.Z;
            return *this;
        }

        template<typename ArgT>
        Point3D<T> operator+=(const ArgT & val){
            Point3D<T> p;
            X += val;
            Y += val;
            Z += val;
            return *this;
        }

        template<typename ArgT>
        Point3D<T> & operator-=(const ArgT & val){
            X -= val;
            Y -= val;
            Z -= val;
            return *this;
        }

        template<typename ArgT>
        Point3D<T> & operator*=(const ArgT & val){
            X *= val;
            Y *= val;
            Z *= val;
            return *this;
        }

        template<typename ArgT>
        Point3D<T> & operator/=(const ArgT & val){
            X /= val;
            Y /= val;
            Z /= val;
            return *this;
        }

        template<typename ArgT>
        Point3D<T> & operator=(const Point3D<ArgT> & point){
            X = point.X;
            Y = point.Y;
            Z = point.Z;
            return *this;
        }

        template<typename ArgT>
        bool operator==(const Point3D<ArgT> & point) const {
            return X==point.X && Y==point.Y && Z==point.Z;
        };

        template<typename ArgT>
        bool operator==(const ArgT & value) const {
            return X==value && Y==value && Z==value;
        };

        template<typename ArgT>
        static T abs(const Point3D<ArgT> & point) {
            return std::sqrt(point.X*point.X+point.Y*point.Y+point.Z*point.Z);
        }


        template<typename ArgT>
        static T dot(const Point3D<ArgT> & p1, const Point3D<ArgT> & p2){
            return p1.X*p2.X+p1.Y*p2.Y+p1.Z*p2.Z;
        }

        template<typename ArgTIn>
        static Point3D<T> cross(const Point3D<ArgTIn> & p1, const Point3D<ArgTIn> & p2){
            return Point3D<T>{
                    (static_cast<T>(p1.Y)*static_cast<T>(p2.Z) - static_cast<T>(p1.Z)*static_cast<T>(p2.Y)),
                    (static_cast<T>(p1.Z)*static_cast<T>(p2.X) - static_cast<T>(p1.X)*static_cast<T>(p2.Z)),
                    (static_cast<T>(p1.X)*static_cast<T>(p2.Y) - static_cast<T>(p1.Y)*static_cast<T>(p2.X))
            };
        }

        template<typename ArgT>
        static T cos(const Point3D<ArgT> & p1, const Point3D<ArgT> & p2){
            return (dot(p1, p2))/((abs(p1)*abs(p2)));
        }

        template<typename ArgT>
        static T sin(const Point3D<ArgT> & p1, const Point3D<ArgT> & p2){
            return (abs(cross(p1, p2)))/((abs(p1)*abs(p2)));
        }

        template<typename ArgT>
        static T signed_sin(const Point3D<ArgT> & p1, const Point3D<ArgT> & p2, const Point3D<ArgT> & dir){
            return cos(p1,dir) > cos(p2,dir) ? sin(p1,p2) : -sin(p1,p2);
        }

        struct Tilt {
                float Pitch;
                float Roll;
        };

        //needs point3d that is oriented in a way where Z axis gives -1g in default position
        static Tilt tilt(const Point3D<T> & p) {
            return Tilt{
                atan(p.X/sqrt(p.Y*p.Y+p.Z*p.Z))* 180 / pi,
                atan(p.Y/sqrt(p.X*p.X+p.Z*p.Z))* 180 / pi
            };
        }


        T X;
        T Y;
        T Z;
    };


}


#endif //OPENBREWERYFW_LIB_GEOMETRY_H
