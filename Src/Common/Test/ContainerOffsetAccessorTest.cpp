//
// Created by mchodzikiewicz on 09.12.18.
//

#include "ContainerOffsetAccessor.h"
#include "etl/vector.h"
#include "gtest/gtest.h"


class ContainerOffsetAccessorTest : public ::testing::Test {
public:
    void SetUp() override {

    };
    void TearDown() override {

    };



};

TEST_F(ContainerOffsetAccessorTest, FrontOffset) {
    etl::vector<uint8_t,10> vector;
    ContainerOffsetAccessor<decltype(vector),1,0> accessor(vector);
    vector.push_back(1);
    vector.push_back(2);
    vector.push_back(3);
    vector.push_back(4);
    EXPECT_EQ(vector.at(0),1);
    EXPECT_EQ(accessor.at(0),2);
    EXPECT_EQ(accessor.at(1),3);
    EXPECT_EQ(accessor.at(2),4);
    EXPECT_EQ(accessor.begin(),vector.begin()+1);
    EXPECT_EQ(accessor.end(),vector.end());
}


TEST_F(ContainerOffsetAccessorTest,BackOffset){
    etl::vector<uint8_t,10> vector;
    ContainerOffsetAccessor<decltype(vector),0,1> accessor(vector);
    EXPECT_EQ(accessor.begin(),vector.begin());
    EXPECT_EQ(accessor.end(),vector.end()-1);
}

TEST_F(ContainerOffsetAccessorTest, PushBack1Offset){
    etl::vector<uint8_t,10> vector;
    ContainerOffsetAccessor<decltype(vector),1,0> accessor(vector);
    accessor.push_back(1);
    accessor.push_back(2);
    accessor.push_back(3);
    accessor.push_back(4);
    accessor.push_back(5);
    EXPECT_EQ(accessor.at(0),1);
    EXPECT_EQ(accessor.at(1),2);
    EXPECT_EQ(accessor.at(2),3);
    EXPECT_EQ(accessor.at(3),4);
    EXPECT_EQ(accessor.at(4),5);

}


TEST_F(ContainerOffsetAccessorTest, PushBack5OffsetEmptyVector){
    etl::vector<uint8_t,12> vector;
    ContainerOffsetAccessor<decltype(vector),5,0> accessor(vector);
    accessor.push_back(1);
    accessor.push_back(2);
    accessor.push_back(3);
    accessor.push_back(4);
    accessor.push_back(5);
    EXPECT_EQ(accessor.at(0),1);
    EXPECT_EQ(accessor.at(1),2);
    EXPECT_EQ(accessor.at(2),3);
    EXPECT_EQ(accessor.at(3),4);
    EXPECT_EQ(accessor.at(4),5);

}

TEST_F(ContainerOffsetAccessorTest, PushBack5OffsetPartiallyPrefilledVector){
    etl::vector<uint8_t,12> vector;
    ContainerOffsetAccessor<decltype(vector),5,0> accessor(vector);
    vector.push_back(9);
    vector.push_back(9);
    accessor.push_back(1);
    accessor.push_back(2);
    accessor.push_back(3);
    accessor.push_back(4);
    accessor.push_back(5);
    EXPECT_EQ(accessor.at(0),1);
    EXPECT_EQ(accessor.at(1),2);
    EXPECT_EQ(accessor.at(2),3);
    EXPECT_EQ(accessor.at(3),4);
    EXPECT_EQ(accessor.at(4),5);

}