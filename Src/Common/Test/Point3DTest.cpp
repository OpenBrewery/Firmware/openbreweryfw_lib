//
// Created by mchodzikiewicz on 10/13/18.
//

#include "Point3D.h"
#include "etl/vector.h"

#include "gtest/gtest.h"


class Point3DTest : public ::testing::Test {
public:
    void SetUp() override {

    };
    void TearDown() override {

    };

};

TEST_F(Point3DTest, InitNoArgs) {
    Geometry::Point3D<int> point;
    EXPECT_EQ(point.X,0);
    EXPECT_EQ(point.Y,0);
    EXPECT_EQ(point.Z,0);
    EXPECT_EQ(point,0);
}

TEST_F(Point3DTest, InitWithArgs) {
    Geometry::Point3D<int> point(1,2,3);
    EXPECT_EQ(point.X,1);
    EXPECT_EQ(point.Y,2);
    EXPECT_EQ(point.Z,3);
}

TEST_F(Point3DTest, Reorientate) {
    EXPECT_EQ(Geometry::Point3D<int>(100,0,-1000)
             ,Geometry::Point3D<int>::reorientate(Geometry::Point3D<int>(0,-1000,100)
                    ,Geometry::Point3D<int>::Origin::Y_negative));
    EXPECT_EQ(Geometry::Point3D<int>(100,0,1000)
    ,Geometry::Point3D<int>::reorientate(Geometry::Point3D<int>(100,0,1000)
            ,Geometry::Point3D<int>::Origin::Z_negative));
}

TEST_F(Point3DTest, Add) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<int> p2(10,20,30);
    EXPECT_EQ(p1+p2,Geometry::Point3D<int>(11,22,33));
}

TEST_F(Point3DTest, AddWithCast) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<float> p2(10,20,30);
    EXPECT_EQ(p1+p2,Geometry::Point3D<int>(11,22,33));
    EXPECT_EQ(p2+p1,Geometry::Point3D<float>(11,22,33));
}

TEST_F(Point3DTest, Substract) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<int> p2(10,20,30);
    EXPECT_EQ(p2-p1,Geometry::Point3D<int>(9,18,27));
    EXPECT_EQ(p1-p2,Geometry::Point3D<int>(-9,-18,-27));
}

TEST_F(Point3DTest, SubstractWithCast) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<float> p2(10,20,30);
    EXPECT_EQ(p2-p1,Geometry::Point3D<float>(9,18,27));
    EXPECT_EQ(p1-p2,Geometry::Point3D<int>(-9,-18,-27));
}

TEST_F(Point3DTest, Multiply) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<int> p2(10,20,30);
    EXPECT_EQ(p1*p2,Geometry::Point3D<int>(10,40,90));

}

TEST_F(Point3DTest, MultiplyWithCast) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<float> p2(10,20,30);
    EXPECT_EQ(p1*p2,Geometry::Point3D<int>(10,40,90));
    EXPECT_EQ(p2*p1,Geometry::Point3D<float>(10,40,90));
}

TEST_F(Point3DTest, Divide) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<int> p2(10,20,30);
    EXPECT_EQ(p2/p1,Geometry::Point3D<int>(10,10,10));
    EXPECT_EQ(p1/p2,Geometry::Point3D<int>(0,0,0));
}

TEST_F(Point3DTest, DivideWithCast) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<float> p2(10,20,30);
    EXPECT_EQ(p2/p1,Geometry::Point3D<float>(10,10,10));
    EXPECT_EQ(p1/p2,Geometry::Point3D<int>(0,0,0));
}

TEST_F(Point3DTest, Assignment) {
    Geometry::Point3D<int> p1;
    Geometry::Point3D<int> p2(1,2,3);
    EXPECT_EQ(Geometry::Point3D<int>(1,2,3),p1 = p2);
    EXPECT_EQ(Geometry::Point3D<int>(1,2,3),p1);
}

TEST_F(Point3DTest, AssignmentWithConversion) {
    Geometry::Point3D<int> p1;
    Geometry::Point3D<float> p2(1,2,3);
    EXPECT_EQ(Geometry::Point3D<int>(1,2,3),p1 = p2);
    EXPECT_EQ(Geometry::Point3D<int>(1,2,3),p1);
}

TEST_F(Point3DTest, CopyConstructor) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<int> p2(p1);
    EXPECT_EQ(Geometry::Point3D<int>(1,2,3),p2);
}

TEST_F(Point3DTest, CopyConstructorWithCast) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<float> p2(p1);
    EXPECT_EQ(Geometry::Point3D<float>(1,2,3),p2);
}

TEST_F(Point3DTest, PlusEqual) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<int> p2(p1);
    EXPECT_EQ(Geometry::Point3D<int>(2,4,6),p1+=p2);
    EXPECT_EQ(Geometry::Point3D<int>(2,4,6),p1);
}

TEST_F(Point3DTest, PlusEqualWithCast) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<float> p2(p1);
    EXPECT_EQ(Geometry::Point3D<int>(2,4,6),p1+=p2);
    EXPECT_EQ(Geometry::Point3D<int>(2,4,6),p1);
}

TEST_F(Point3DTest, MinusEqual) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<int> p2(p1);
    EXPECT_EQ(Geometry::Point3D<int>(0,0,0),p1-=p2);
    EXPECT_EQ(Geometry::Point3D<int>(0,0,0),p1);
}

TEST_F(Point3DTest, MinusEqualWithCast) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<float> p2(p1);
    EXPECT_EQ(Geometry::Point3D<int>(0,0,0),p1-=p2);
    EXPECT_EQ(Geometry::Point3D<int>(0,0,0),p1);
}


TEST_F(Point3DTest, TimesEqual) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<int> p2(p1);
    EXPECT_EQ(Geometry::Point3D<int>(1,4,9),p1*=p2);
    EXPECT_EQ(Geometry::Point3D<int>(1,4,9),p1);
}

TEST_F(Point3DTest, TimesEqualWithCast) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<float> p2(p1);
    EXPECT_EQ(Geometry::Point3D<int>(1,4,9),p1*=p2);
    EXPECT_EQ(Geometry::Point3D<int>(1,4,9),p1);
}


TEST_F(Point3DTest, DivideEqual) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<int> p2(p1);
    EXPECT_EQ(Geometry::Point3D<int>(1,1,1),p1/=p2);
    EXPECT_EQ(Geometry::Point3D<int>(1,1,1),p1);
}

TEST_F(Point3DTest, DivideEqualWithCast) {
    Geometry::Point3D<int> p1(1,2,3);
    Geometry::Point3D<float> p2(p1);
    EXPECT_EQ(Geometry::Point3D<int>(1,1,1),p1/=p2);
    EXPECT_EQ(Geometry::Point3D<int>(1,1,1),p1);
}


TEST_F(Point3DTest, PlusEqualInt) {
    Geometry::Point3D<int> p1(1,2,3);
    EXPECT_EQ(Geometry::Point3D<int>(6,7,8),p1+=5);
    EXPECT_EQ(Geometry::Point3D<int>(6,7,8),p1);
}

TEST_F(Point3DTest, PlusEqualFloat) {
    Geometry::Point3D<float> p1(1,2,3);
    EXPECT_EQ(Geometry::Point3D<float>(6.5,7.5,8.5),p1+=5.5);
    EXPECT_EQ(Geometry::Point3D<float>(6.5,7.5,8.5),p1);
}

TEST_F(Point3DTest, MinusEqualInt) {
    Geometry::Point3D<int> p1(1,2,3);
    EXPECT_EQ(Geometry::Point3D<int>(0,1,2),p1-=1);
    EXPECT_EQ(Geometry::Point3D<int>(0,1,2),p1);
}

TEST_F(Point3DTest, MinusEqualFloat) {
    Geometry::Point3D<float> p1(1,2,3);
    EXPECT_EQ(Geometry::Point3D<float>(0.5,1.5,2.5),p1-=0.5);
    EXPECT_EQ(Geometry::Point3D<float>(0.5,1.5,2.5),p1);
}

TEST_F(Point3DTest, TimesEqualInt) {
    Geometry::Point3D<int> p1(1,2,3);
    EXPECT_EQ(Geometry::Point3D<int>(2,4,6),p1*=2);
    EXPECT_EQ(Geometry::Point3D<int>(2,4,6),p1);
}

TEST_F(Point3DTest, TimesEqualFloat) {
    Geometry::Point3D<int> p1(1,2,3);
    EXPECT_EQ(Geometry::Point3D<int>(2,5,7),p1*=2.5);
    EXPECT_EQ(Geometry::Point3D<int>(2,5,7),p1);
}

TEST_F(Point3DTest, DivideEqualInt) {
    Geometry::Point3D<int> p1(2,4,6);
    EXPECT_EQ(Geometry::Point3D<int>(1,2,3),p1/=2);
    EXPECT_EQ(Geometry::Point3D<int>(1,2,3),p1);
}

TEST_F(Point3DTest, DivideEqualFloat) {
    Geometry::Point3D<float> p1(2,4,6);
    EXPECT_EQ(Geometry::Point3D<float>(1,2,3),p1/=2);
    EXPECT_EQ(Geometry::Point3D<float>(1,2,3),p1);
}


TEST_F(Point3DTest, AvgOf1Element) {
    etl::vector<Geometry::Point3D<int>,1> vec{{2,4,6}};
    EXPECT_EQ(Geometry::Point3D<int>(2,4,6),Geometry::Point3D<int>::avg(etl::array_view<Geometry::Point3D<int>>(vec)));
}

TEST_F(Point3DTest, AvgOf2Elements) {
    etl::vector<Geometry::Point3D<int>,2> vec{{2,4,6},{0,0,0}};
    EXPECT_EQ(Geometry::Point3D<int>(1,2,3),Geometry::Point3D<int>::avg(etl::array_view<Geometry::Point3D<int>>(vec)));
}

TEST_F(Point3DTest, AvgNegative) {
    etl::vector<Geometry::Point3D<int>,100> vec;
    for(int i=0; i<100; i++) {
        vec.push_back({-10000, 0, 0});
    }
    EXPECT_EQ(Geometry::Point3D<int>(-10000,0,0),Geometry::Point3D<int>::avg(etl::array_view<Geometry::Point3D<int>>(vec)));
    vec.clear();
    for(int i=0; i<100; i++) {
        vec.push_back({0, -10000, 0});
    }
    EXPECT_EQ(Geometry::Point3D<int>(0,-10000,0),Geometry::Point3D<int>::avg(etl::array_view<Geometry::Point3D<int>>(vec)));
    vec.clear();
    for(int i=0; i<100; i++) {
        vec.push_back({0, 0, -10000});
    }
    EXPECT_EQ(Geometry::Point3D<int>(0,0,-10000),Geometry::Point3D<int>::avg(etl::array_view<Geometry::Point3D<int>>(vec)));
}

TEST_F(Point3DTest, AvgOf2ElementsNegative) {
    etl::vector<Geometry::Point3D<int>,2> vec{{2,-4,6},{0,0,0}};
    EXPECT_EQ(Geometry::Point3D<int>(1,-2,3),Geometry::Point3D<int>::avg(etl::array_view<Geometry::Point3D<int>>(vec)));
}

TEST_F(Point3DTest, Abs){
    EXPECT_EQ(1,Geometry::Point3D<int>::abs(Geometry::Point3D<int>{1,0,0}));
    EXPECT_FLOAT_EQ(sqrt(2),Geometry::Point3D<float>::abs(Geometry::Point3D<int>{1,1,0}));
    EXPECT_FLOAT_EQ(sqrt(3),Geometry::Point3D<float>::abs(Geometry::Point3D<int>{1,1,1}));
    EXPECT_FLOAT_EQ(sqrt(2)*5,Geometry::Point3D<float>::abs(Geometry::Point3D<int>{5,5,0}));
}

TEST_F(Point3DTest, Dot){
    EXPECT_EQ(1,Geometry::Point3D<int>::dot(Geometry::Point3D<int>{1,0,0},Geometry::Point3D<int>{1,0,0}));
    EXPECT_EQ(2,Geometry::Point3D<int>::dot(Geometry::Point3D<int>{1,1,0},Geometry::Point3D<int>{1,1,0}));
    EXPECT_EQ(3,Geometry::Point3D<int>::dot(Geometry::Point3D<int>{1,1,1},Geometry::Point3D<int>{1,1,1}));
    EXPECT_EQ(12,Geometry::Point3D<int>::dot(Geometry::Point3D<int>{2,2,2},Geometry::Point3D<int>{2,2,2}));
}

TEST_F(Point3DTest, Cross){
    EXPECT_EQ(Geometry::Point3D<int>::cross(Geometry::Point3D<int>{-2,-1,-1},Geometry::Point3D<int>{-5,-5,-5}),Geometry::Point3D<int>(0,-5,5));
    EXPECT_EQ(Geometry::Point3D<int>::cross(Geometry::Point3D<int>{-3,4,-3},Geometry::Point3D<int>{-3,3,-2}),Geometry::Point3D<int>(1,3,3));
    EXPECT_EQ(Geometry::Point3D<int>::cross(Geometry::Point3D<int>{-4,0,-1},Geometry::Point3D<int>{4,-5,-1}),Geometry::Point3D<int>(-5,-8,20));
    EXPECT_EQ(Geometry::Point3D<int>::cross(Geometry::Point3D<int>{-4,-2,1},Geometry::Point3D<int>{2,-3,4}),Geometry::Point3D<int>(-5,18,16));
}

TEST_F(Point3DTest, Cosine){
    EXPECT_EQ(1,Geometry::Point3D<float>::cos(Geometry::Point3D<int>{1,0,0},Geometry::Point3D<int>{1,0,0}));
    EXPECT_EQ(0,Geometry::Point3D<float>::cos(Geometry::Point3D<int>{1,0,0},Geometry::Point3D<int>{0,1,0}));
    EXPECT_EQ(-1,Geometry::Point3D<float>::cos(Geometry::Point3D<int>{1,0,0},Geometry::Point3D<int>{-1,0,0}));
    EXPECT_FLOAT_EQ(sqrt(2)/2,Geometry::Point3D<float>::cos(Geometry::Point3D<int>{1,0,0},Geometry::Point3D<int>{1,1,0}));

    //TODO (mchodzikiewicz): it would be nice to test it in more cases
}

TEST_F(Point3DTest, Sine){
    EXPECT_EQ(0,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{1,0,0},Geometry::Point3D<int>{1,0,0}));
    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{1,0,0},Geometry::Point3D<int>{0,1,0}));
    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{1,0,0},Geometry::Point3D<int>{0,0,1}));

    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,1,0},Geometry::Point3D<int>{1,0,0}));
    EXPECT_EQ(0,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,1,0},Geometry::Point3D<int>{0,1,0}));
    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,1,0},Geometry::Point3D<int>{0,0,1}));

    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,0,1},Geometry::Point3D<int>{1,0,0}));
    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,0,1},Geometry::Point3D<int>{0,1,0}));
    EXPECT_EQ(0,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,0,1},Geometry::Point3D<int>{0,0,1}));


    EXPECT_EQ(0,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{1,0,0},Geometry::Point3D<int>{-1,0,0}));
    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{1,0,0},Geometry::Point3D<int>{0,-1,0}));
    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{1,0,0},Geometry::Point3D<int>{0,0,-1}));

    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,1,0},Geometry::Point3D<int>{-1,0,0}));
    EXPECT_EQ(0,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,1,0},Geometry::Point3D<int>{0,-1,0}));
    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,1,0},Geometry::Point3D<int>{0,0,-1}));

    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,0,1},Geometry::Point3D<int>{-1,0,0}));
    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,0,1},Geometry::Point3D<int>{0,-1,0}));
    EXPECT_EQ(0,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,0,1},Geometry::Point3D<int>{0,0,-1}));


    EXPECT_EQ(0,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{1,0,0},Geometry::Point3D<int>{2,0,0}));
    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{1,0,0},Geometry::Point3D<int>{0,2,0}));
    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{1,0,0},Geometry::Point3D<int>{0,0,2}));

    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,1,0},Geometry::Point3D<int>{2,0,0}));
    EXPECT_EQ(0,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,1,0},Geometry::Point3D<int>{0,2,0}));
    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,1,0},Geometry::Point3D<int>{0,0,2}));

    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,0,1},Geometry::Point3D<int>{2,0,0}));
    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,0,1},Geometry::Point3D<int>{0,2,0}));
    EXPECT_EQ(0,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,0,1},Geometry::Point3D<int>{0,0,2}));


    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{1,0,0},Geometry::Point3D<int>{0,1,1}));
    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,1,0},Geometry::Point3D<int>{1,0,1}));
    EXPECT_EQ(1,Geometry::Point3D<float>::sin(Geometry::Point3D<int>{0,0,1},Geometry::Point3D<int>{1,1,0}));
}

TEST_F(Point3DTest,SignedSine){
    EXPECT_FLOAT_EQ(-0.28,Geometry::Point3D<float>::signed_sin(Geometry::Point3D<int>{3,0,-4},Geometry::Point3D<int>{4,0,-3},Geometry::Point3D<int>{0,0,1}));
    EXPECT_FLOAT_EQ(-0.28,Geometry::Point3D<float>::signed_sin(Geometry::Point3D<int>{-3,0,-4},Geometry::Point3D<int>{-4,0,-3},Geometry::Point3D<int>{0,0,1}));

    EXPECT_FLOAT_EQ(0.28,Geometry::Point3D<float>::signed_sin(Geometry::Point3D<int>{4,0,-3},Geometry::Point3D<int>{3,0,-4},Geometry::Point3D<int>{0,0,1}));
    EXPECT_FLOAT_EQ(0.28,Geometry::Point3D<float>::signed_sin(Geometry::Point3D<int>{-4,0,-3},Geometry::Point3D<int>{-3,0,-4},Geometry::Point3D<int>{0,0,1}));


    EXPECT_FLOAT_EQ(-0.28,Geometry::Point3D<float>::signed_sin(Geometry::Point3D<int>{0,3,-4},Geometry::Point3D<int>{0,4,-3},Geometry::Point3D<int>{0,0,1}));
    EXPECT_FLOAT_EQ(-0.28,Geometry::Point3D<float>::signed_sin(Geometry::Point3D<int>{0,-3,-4},Geometry::Point3D<int>{0,-4,-3},Geometry::Point3D<int>{0,0,1}));

    EXPECT_FLOAT_EQ(0.28,Geometry::Point3D<float>::signed_sin(Geometry::Point3D<int>{0,4,-3},Geometry::Point3D<int>{0,3,-4},Geometry::Point3D<int>{0,0,1}));
    EXPECT_FLOAT_EQ(0.28,Geometry::Point3D<float>::signed_sin(Geometry::Point3D<int>{0,-4,-3},Geometry::Point3D<int>{0,-3,-4},Geometry::Point3D<int>{0,0,1}));
}

//TEST_F(Point3DTest,Tilt){
//    auto tilt =
//    EXPECT_FLOAT_EQ()
//}