# OpenBrewery Firmware Library

This project is intended to provide readymade toolbox for embedded devices for OpenBrewery project.

It is intended to be lightweight, platform agnostic modern C++ framework for IoT nodes, providing reach IO interfaces for both, hardware and communication stacks.
Since topics are related somehow, it should suite process regulation devices etc.

It is meant to be well tested (TDD & CI) and easily integrable with other projects (CMake)


### Story behind:
Since this library is intended to be used in wide range of embedded use cases as:
* application on application processor with full-blown OS (ie. Linux on Cortex-A)
* application based on some kind of lightweight RTOS (ie. FreeRTOS on Cortex-M0 / ESP32 with esp-idf)
* bare-metal application without threads (ie. Cortex-M0+)

it needs to address all the limitations without compomising clearance of target application.

It means that in cases where compromise between simple API and performance / memory footprint cannot be found, more than one solution is provided.
