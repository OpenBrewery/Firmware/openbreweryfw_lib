
cmake_minimum_required(VERSION 3.10)
project(OpenBreweryFW_lib)
set(CMAKE_CXX_STANDARD 17)

option(TEST_OPENBREWERY_FW_LIB   "Build unit tests" OFF)
option(BUILD_MOCKS               "Build mocks"      OFF)

if(NOT TARGET etl)
        add_subdirectory(etl)
endif()


add_library(openbreweryfw_lib
        Src/Peripherals/Src/DigitalInput.cpp
        Src/Peripherals/Src/DigitalOutput.cpp
        Src/Peripherals/Src/AnalogInput.cpp
        Src/Peripherals/Src/AnalogOutput.cpp
        Src/Sensors/Src/MMA8452.cpp
        Src/Peripherals/Src/CommInterface.cpp
        Src/Sensors/Src/SHT21.cpp
        Src/Peripherals/Src/Wifi.cpp
        )

target_include_directories(openbreweryfw_lib PUBLIC
        etl/include
        Src/Common/Inc
        Src/Peripherals/Inc/
        Src/Sensors/Inc/
        Src/Communication/Inc
        Src/System/Inc
)

target_link_libraries(openbreweryfw_lib
        PUBLIC
        etl)


if(BUILD_MOCKS OR TEST_OPENBREWERY_FW_LIB)
    add_library(openbreweryfw_lib_mocks
            Src/Communication/Test/DummyHttpClient.cpp
            Src/Communication/Test/DummyHttpServer.cpp
            Src/Peripherals/Test/DummyAsyncInterface.cpp
            Src/Peripherals/Test/DummyCommInterface.cpp
            Src/Peripherals/Test/DummyTransactionInterface.cpp
            Src/Peripherals/Test/DummyWifiClient.cpp
            Src/Peripherals/Test/DummyWifiServer.cpp
            Src/System/Test/DummySystem.cpp
            Src/System/Test/DummyRegistry.cpp
            Src/System/Test/DummyFirmwareManager.cpp
            )

    target_include_directories(openbreweryfw_lib_mocks
            PUBLIC
            Src/Communication/Test/
            Src/Peripherals/Test/
            Src/System/Test/
            )
    target_link_libraries(openbreweryfw_lib_mocks PRIVATE openbreweryfw_lib)



endif()

if(TEST_OPENBREWERY_FW_LIB)
        if(NOT TARGET gtest)
                add_subdirectory(googletest)
        endif()

        add_executable(openbreweryfw_lib_test
                Src/Test/main.cpp
                Src/Common/Test/Point3DTest.cpp
                Src/Common/Test/ContainerOffsetAccessorTest.cpp
                Src/Communication/Test/DummyHttpClientTest.cpp
                Src/Peripherals/Test/AnalogOutputTest.cpp
                Src/Peripherals/Test/DummyInterfaceTest.cpp
                Src/Peripherals/Test/DummyMappedDeviceTest.cpp
                Src/Peripherals/Test/I2CInterfaceTest.cpp
                Src/Peripherals/Test/SerialPortTest.cpp
                Src/Sensors/Test/AccelerometerTest.cpp
                Src/Sensors/Test/MMA8452Test.cpp
                Src/Sensors/Test/SHT21Test.cpp
                Src/Peripherals/Test/WifiClientTest.cpp
                Src/Sensors/Test/SensorSimulatorTest.cpp
                Src/Peripherals/Test/WifiTest.cpp
                Src/Peripherals/Test/WifiServerTest.cpp
                Src/Communication/Test/DummyHttpServerTest.cpp
                Src/System/Test/SystemTest.cpp
                Src/System/Test/RegistryTest.cpp
                Src/System/Test/FirmwareManagerTest.cpp
                )


        target_include_directories(openbreweryfw_lib_test
                PUBLIC
                Src/Common/Test
                Src/Communication/Test
                Src/Peripherals/Test
                Src/Sensors/Test
                Src/System/Test
                )
        target_link_libraries(openbreweryfw_lib_test openbreweryfw_lib openbreweryfw_lib_mocks gtest gtest_main gmock)

endif()