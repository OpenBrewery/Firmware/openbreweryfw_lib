You are welcome to contribute a code to the project!
Until explicit code convention and tools will be written, please adapt your style to one that exists in the code already.

Project should compile and test itself out of box in any environment that supports CMake.
It is very smooth to develop it in CLion.